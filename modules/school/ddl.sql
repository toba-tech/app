SET search_path TO template;

CREATE TABLE teachers (
   user_id UUID PRIMARY KEY NOT NULL,
   properties JSONB
);
ALTER TABLE teachers ADD FOREIGN KEY (user_id) REFERENCES users (id);


CREATE TABLE students (
   id SERIAL PRIMARY KEY NOT NULL,
   first_name INT,
   last_name INT,
   school_id TEXT
);


CREATE TABLE assignments (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL,
   description VARCHAR,
   due_on DATE,
   teacher_id UUID NOT NULL
);
ALTER TABLE assignments ADD FOREIGN KEY (teacher_id) REFERENCES teachers (user_id);


CREATE TABLE student_assignments (
   student_id INT NOT NULL,
   assignment_id INT NOT NULL,
   score NUMERIC DEFAULT 0 NOT NULL,
   turned_in_on TIMESTAMPTZ,
   CONSTRAINT student_assignments_pk PRIMARY KEY (student_id, assignment_id)
);
ALTER TABLE student_assignments ADD FOREIGN KEY (student_id) REFERENCES students (id);
ALTER TABLE student_assignments ADD FOREIGN KEY (assignment_id) REFERENCES assignments (id);


CREATE TABLE student_subscribers (
   user_id UUID NOT NULL,
   student_id INT NOT NULL,
   relation_id INT,
   CONSTRAINT student_subscribers_pk PRIMARY KEY (user_id, student_id)
);
ALTER TABLE student_subscribers ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE student_subscribers ADD FOREIGN KEY (student_id) REFERENCES students (id);
ALTER TABLE student_subscribers ADD FOREIGN KEY (relation_id) REFERENCES relations (id);


