import * as React from "react";
import Logo from "jsx/logo";
import Icon from "jsx/icon";
import { Action } from "modules/actions";
import FluxComponent from "jsx/flux";
import Security from "./security";
import License from "./license";
import Storage from "./storage";
import Auth from "./auth";
import { Tab, Tabs } from "jsx/tabs";
import { setupStore, SetupState } from "./store";
import text from "./localize";
import { osPrefix } from "lib/i18n/language";

const tabs:Tab[] = [
   {
      title: text.LICENSE,
      icon: "description",
      view: License
   },
   {
      title: text.SECURITY,
      icon: "security",
      view: Security
   },
   {
      title: text.STORAGE,
      icon: "storage",
      view: Storage
   },
   {
      title: text.AUTH,
      icon: "lock_outline",
      view: Auth
   }
];

export default class extends FluxComponent<{}, SetupState> {
   constructor(props:any) {
      super(props, setupStore);
      this.emit(Action.GetHostConfiguration);
      this.selectTab = this.selectTab.bind(this);
   }

   selectTab(index:number) {
      this.setState({ selectedTab: index });
   }

   render() {
      return <div id="setup">
         <Logo/>
         <h1>{ text.WELCOME }</h1>
         <h2>{ text.DETECTED_CONFIG }</h2>

         <div className="columns">
            <dl>
               <dt>{ text.LICENSE }<Icon onClick={()=>this.selectTab(0)} name="settings"/></dt>
               <dd>{ this.state.licensed ? text.ACTIVE : text.TRIAL }</dd>

               <dt>User</dt>
               <dd>{ this.state.processUser }</dd>

               <dt>{ text.SERVER_OPERATING_SYSTEM }</dt>
               <dd>{ text[osPrefix + this.state.os] }</dd>
            </dl>
            <dl>
               <dt>{ text.APP_PATH }<Icon onClick={()=>this.selectTab(2)} name="settings"/></dt>
               <dd className="path">{ this.state.appPath }</dd>

               <dt>{ text.DATA_PATH }</dt>
               <dd className="path">{ this.state.dataPath }</dd>
            </dl>
         </div>
         <Tabs tabs={tabs} selected={this.state.selectedTab}/>
      </div>;
   }
}