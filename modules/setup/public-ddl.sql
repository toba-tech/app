CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE IF NOT EXISTS message_queue (
   id SERIAL PRIMARY KEY NOT NULL,
   table_name TEXT NOT NULL,
   schema_name TEXT NOT NULL,
   operation TEXT NOT NULL,
   new_val JSONB,
   old_val JSONB
);
COMMENT ON TABLE message_queue IS 'Triggered inserts consolidate table changes to notify connections';

CREATE TABLE IF NOT EXISTS startup_logs (
   id SERIAL PRIMARY KEY NOT NULL,
   host_name TEXT NOT NULL,
   at TIMESTAMPTZ DEFAULT now() NOT NULL,
   architecture TEXT NOT NULL,
   platform TEXT NOT NULL,
   node_version TEXT NOT NULL,
   os_version TEXT NOT NULL,
   total_memory NUMERIC NOT NULL DEFAULT 0,
   cpu_count SMALLINT NOT NULL DEFAULT 0,
   cpu_info JSONB,
   network_interfaces JSONB
);

CREATE TABLE IF NOT EXISTS public.server_logs (
   id BIGSERIAL PRIMARY KEY NOT NULL,
   pid INT NOT NULL,
   type SMALLINT NOT NULL,
   at TIMESTAMPTZ DEFAULT now() NOT NULL,
   message TEXT NOT NULL,
   properties JSONB
);

CREATE TABLE IF NOT EXISTS tasks (
   id SMALLSERIAL PRIMARY KEY NOT NULL,
   description TEXT NOT NULL,
   procedure TEXT NOT NULL,
   enabled BOOLEAN NOT NULL DEFAULT TRUE,
   created_on TIMESTAMPTZ DEFAULT now() NOT NULL,
   interval_minutes SMALLINT NOT NULL DEFAULT 1440 CONSTRAINT positive CHECK (interval_minutes > 0)
);
COMMENT ON TABLE tasks IS 'Parameterless procedures to be executed by application server';

CREATE TABLE IF NOT EXISTS task_logs (
   task_id SMALLINT NOT NULL,
   start TIMESTAMPTZ NOT NULL,
   finish TIMESTAMPTZ NOT NULL,
   CONSTRAINT task_logs_pk PRIMARY KEY (task_id, start)
);
ALTER TABLE public.task_logs ADD FOREIGN KEY (task_id) REFERENCES public.tasks (id);

CREATE TABLE IF NOT EXISTS platform (
   -- constraint ensure id is a power of 2
   -- http://stackoverflow.com/questions/600293/how-to-check-if-a-number-is-a-power-of-2
   id SMALLINT PRIMARY KEY NOT NULL CONSTRAINT bitmask CHECK (id > 0 AND (id & (id - 1)) = 0),
   name TEXT NOT NULL
);
COMMENT ON COLUMN platform.id IS 'Bitmask value must be power of 2';
INSERT INTO platform (id, name) VALUES
   (1, 'On Premise'),
   (2, 'Cloud');

CREATE TABLE IF NOT EXISTS status (
   id SMALLINT PRIMARY KEY NOT NULL,
   name TEXT NOT NULL
);
INSERT INTO status (id, name) VALUES
   (0, 'disabled'),
   (1, 'enabled'),
   (2, 'pending'),
   (3, 'expired')
ON CONFLICT (id) DO UPDATE SET name = EXCLUDED.name;

CREATE TABLE IF NOT EXISTS modules (
   id SMALLINT PRIMARY KEY NOT NULL,
   name TEXT NOT NULL,
   slug TEXT NOT NULL,
   status SMALLINT NOT NULL DEFAULT 0,
   -- bitmask supports both platforms by default
   platform SMALLINT NOT NULL DEFAULT 1 | 2,
   properties JSONB
);
CREATE UNIQUE INDEX modules_slug_unique ON public.modules (slug);
ALTER TABLE public.modules ADD CONSTRAINT modules_status_id_fk FOREIGN KEY (status) REFERENCES public.status (id);
INSERT INTO public.modules (id, name, slug, platform) VALUES
   ( 10,'Configuration Database', 'config', 1|2),
   ( 20,'Knowledge Base', 'kb', 1|2),
   ( 30,'Maker','maker', 1|2),
   ( 40,'Providers','provider', 1|2),
   ( 50,'Register','register', 2),
   ( 60,'Time Card','time-card', 1|2),
   ( 70,'Incident Management','incident', 1|2),
   ( 80,'Security','security', 1|2),
   ( 90,'Service Desk','service-desk', 1|2),
   (100,'Setup','setup', 1),
   (110,'Forum','forum', 1|2),
   (120,'Employee Directory','directory', 1|2),
   (130,'System','system', 1|2),
   (140,'School','school', 2)
ON CONFLICT (id) DO UPDATE SET platform = EXCLUDED.platform;

CREATE TABLE IF NOT EXISTS reserved_subdomains (slug TEXT PRIMARY KEY NOT NULL);
COMMENT ON TABLE reserved_subdomains IS 'Names that cannot be registered by users';
INSERT INTO public.reserved_subdomains VALUES
   ('register'),
   ('www')
ON CONFLICT (slug) DO NOTHING;

CREATE TABLE IF NOT EXISTS tenants (
   id SMALLSERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL,
   slug TEXT NOT NULL,
   created_on TIMESTAMPTZ DEFAULT now() NOT NULL,
   status SMALLINT NOT NULL DEFAULT 0
);
ALTER TABLE tenants ADD FOREIGN KEY (status) REFERENCES status (id);
CREATE UNIQUE INDEX tenants_slug_unique ON public.tenants (slug);
CREATE TRIGGER trigger_tenants_genid BEFORE INSERT ON tenants FOR EACH ROW EXECUTE PROCEDURE unique_short_id(8);

CREATE TABLE IF NOT EXISTS licenses (
    tenant_id TEXT NOT NULL,
    module_id INT NOT NULL,
    start_on DATE NOT NULL,
    end_on DATE NOT NULL
);
ALTER TABLE licenses ADD FOREIGN KEY (tenant_id) REFERENCES tenants (id);
ALTER TABLE licenses ADD FOREIGN KEY (module_id) REFERENCES modules (id);

-- Functions ------------------------------------------------------------------

CREATE OR REPLACE FUNCTION get_random_number(INTEGER, INTEGER) RETURNS INTEGER AS $$
DECLARE
    start_int ALIAS FOR $1;
    end_int ALIAS FOR $2;
BEGIN
    RETURN trunc(random() * (end_int-start_int) + start_int);
END;
$$ LANGUAGE 'plpgsql' STRICT;


CREATE OR REPLACE FUNCTION short_id(INTEGER) RETURNS TEXT AS $$
DECLARE
   size ALIAS FOR $1;
   id TEXT;
   possible TEXT;
   max INT;
   i INT;
   rnd INT;
BEGIN
   i := 0;
   id := '';
   possible := 'abcdefghijklmnopqrstuvwxyz0123456789';
   max := LENGTH(possible);

   WHILE i < size LOOP
      rnd := get_random_number(1, max);
      id := id || substring(possible from rnd for 1);
      i := i + 1;
   END LOOP;

   RETURN id;
END;
$$ language 'plpgsql';


CREATE OR REPLACE FUNCTION unique_short_id(INTEGER) RETURNS TRIGGER AS $$
DECLARE
   size ALIAS FOR $1;
   id TEXT;
   qry TEXT;
   found TEXT;
BEGIN
   qry := 'SELECT id FROM ' || quote_ident(TG_TABLE_NAME) || ' WHERE id=';

   LOOP
      id := short_id(size);
      EXECUTE qry || id INTO found;
      IF found IS NULL THEN EXIT; END IF;
   END LOOP;

   NEW.id = id;

   RETURN NEW;
END;
$$ language 'plpgsql';


-- https://www.postgresql.org/docs/current/static/sql-createtrigger.html
CREATE OR REPLACE FUNCTION public.update_message_queue()
   RETURNS trigger
   LANGUAGE plpgsql
AS $$
DECLARE
   old_json JSONB;
   new_json JSONB;
BEGIN
   IF TG_OP IN ('INSERT','UPDATE') THEN new_json = row_to_json(NEW); END IF;
   IF TG_OP IN ('DELETE','UPDATE') THEN old_json = row_to_json(OLD); END IF;

   INSERT INTO public.message_queue (
      schema_name,
      table_name,
      operation,
      old_val,
      new_val
   ) VALUES (
      TG_TABLE_SCHEMA,
      TG_TABLE_NAME,
      TG_OP,
      old_json,
      new_json
   );
   RETURN NEW;
END;
$$;

CREATE OR REPLACE FUNCTION public.message_notify()
   RETURNS trigger
   LANGUAGE plpgsql
AS $$
BEGIN
   PERFORM pg_notify('table_update', row_to_json(NEW)::text);
   RETURN NEW;
END;
$$;

-- Tasks ----------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.truncate_message_queue()
   RETURNS SMALLINT
   LANGUAGE plpgsql
AS $$
DECLARE
   removed_rows SMALLINT DEFAULT 0;
BEGIN
   DELETE FROM public.message_queue
   WHERE id IN (
      SELECT id FROM public.message_queue
      ORDER BY id DESC OFFSET 500
   );
   GET DIAGNOSTICS removed_rows = ROW_COUNT;
   RETURN removed_rows;
END;
$$;

INSERT INTO tasks (description, procedure, interval_minutes)
VALUES ('Remove old messages in queue', 'truncate_message_queue', 2);

CREATE OR REPLACE FUNCTION public.remove_unfinished_registrations()
   RETURNS SMALLINT
   LANGUAGE plpgsql
AS $$
DECLARE
   removed_rows SMALLINT DEFAULT 0;
BEGIN
   DELETE FROM public.tenants
   WHERE status = 2 AND age(now(), created_on) > '1 hour'::interval;

   GET DIAGNOSTICS removed_rows = ROW_COUNT;
   RETURN removed_rows;
END;
$$;

INSERT INTO tasks (description, procedure, interval_minutes)
VALUES ('Remove registrations left in pending state', 'remove_unfinished_registrations', 5);