CREATE SCHEMA IF NOT EXISTS template;
SET search_path TO template;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Users and Authorization ----------------------------------------------------

CREATE TABLE IF NOT EXISTS users (
   id UUID PRIMARY KEY DEFAULT public.uuid_generate_v4(),
   first_name TEXT NOT NULL,
   last_name TEXT NOT NULL,
   properties JSONB
);

CREATE TABLE IF NOT EXISTS roles (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS user_roles (
   user_id UUID NOT NULL,
   role_id INT NOT NULL,
   CONSTRAINT user_roles_pk PRIMARY KEY (user_id, role_id)
);
ALTER TABLE user_roles ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE user_roles ADD FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE;

CREATE TABLE IF NOT EXISTS permissions (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT
);

CREATE TABLE IF NOT EXISTS role_permissions (
   role_id INT NOT NULL,
   permission_id INT NOT NULL,
   CONSTRAINT role_permissions_pk PRIMARY KEY (role_id, permission_id)
);
ALTER TABLE role_permissions ADD FOREIGN KEY (role_id) REFERENCES roles (id);
ALTER TABLE role_permissions ADD FOREIGN KEY (permission_id) REFERENCES permissions (id);

-- Logs and Notifications -----------------------------------------------------

CREATE TABLE IF NOT EXISTS client_logs (
   user_id UUID NOT NULL,
   "on" TIMESTAMPTZ NOT NULL,
   properties JSONB,
   CONSTRAINT client_logs_user_id_on_pk PRIMARY KEY (user_id, "on")
);
ALTER TABLE client_logs ADD FOREIGN KEY (user_id) REFERENCES users (id);

CREATE TABLE IF NOT EXISTS notifications (
   id BIGSERIAL PRIMARY KEY NOT NULL,
   end_on TIMESTAMPTZ NOT NULL,
   start_on TIMESTAMPTZ NOT NULL,
   message TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS user_notification (
   user_id UUID NOT NULL,
   notification_id BIGINT NOT NULL,
   CONSTRAINT user_notification_user_id_notification_id_pk PRIMARY KEY (user_id, notification_id)
);
ALTER TABLE user_notification ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE user_notification ADD FOREIGN KEY (notification_id) REFERENCES notifications (id);

-- Lookups --------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS settings (
   schema_version INT NOT NULL,
   properties JSONB,
   password JSONB,
   locale JSONB
);

CREATE TABLE IF NOT EXISTS status (
   id SMALLINT PRIMARY KEY NOT NULL,
   name TEXT NOT NULL
);
INSERT INTO status (id, name) VALUES
   (0, 'disabled'),
   (1, 'enabled');

CREATE TABLE IF NOT EXISTS unit_types (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL
);

-- Relationships --------------------------------------------------------------

CREATE TABLE IF NOT EXISTS relation_types (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS relations (
   id SERIAL PRIMARY KEY NOT NULL,
   inverse_id INT,
   name TEXT,
   type_id INT
);
ALTER TABLE relations ADD FOREIGN KEY (inverse_id) REFERENCES relations (id);
ALTER TABLE relations ADD FOREIGN KEY (type_id) REFERENCES relation_types (id);