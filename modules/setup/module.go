package setup

import (
	"os"
	"path/filepath"

	"toba.tech/app/lib/db"
	"toba.tech/app/lib/host"

	"toba.tech/app/lib/module"
	"toba.tech/app/modules"
)

type (
	ConfigurationResponse struct {
		AppPath         string               `json:"appPath"`
		DataPath        string               `json:"dataPath"`
		ProcessUser     string               `json:"processUser"`
		Licensed        bool                 `json:"licensed"`
		HasSSL          bool                 `json:"hasSSL"`
		OperatingSystem host.OperatingSystem `json:"os"`
		DomainMember    bool                 `json:"domain"`
	}
)

const (
	GetHostConfiguration module.ServiceID = iota + modules.Setup
)

var (
	// Module defines the services and storage used by the setup module.
	Module = module.
		Register(modules.Setup, "setup").
		RequireLicense(false).
		Add(GetHostConfiguration, module.MakeEndpoint(getConfiguration, nil))
)

// getConfiguration retrieves platform details used to guide setup.
func getConfiguration(req *module.Request) *module.Response {
	res := &ConfigurationResponse{}
	path := ""

	ex, err := os.Executable()
	if err == nil {
		path = filepath.Dir(ex)
	}

	res.AppPath = path
	res.DataPath = db.Path
	res.ProcessUser = "user"
	res.Licensed = false
	res.HasSSL = false
	res.OperatingSystem = host.HostOS
	return module.Success(res)
}
