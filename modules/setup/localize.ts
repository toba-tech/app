import { translate, PhraseList, Translation } from "lib/i18n/localize";

interface Phrases extends Translation {
   ACTIVE:string;
   APP_NAME:string;
   APP_PATH:string;
   AUTH:string;
   DATA_PATH:string;
   DETECTED_CONFIG:string;
   LICENSE:string;
   SECURITY:string;
   SERVER_OPERATING_SYSTEM:string;
   STORAGE:string;
   TRIAL:string;
   WELCOME:string;
}

export default translate<Phrases>({
   ACTIVE: {
      en: "Active"
   },
   APP_NAME: {
      en: "Setup"
   },
   APP_PATH: {
      en: "Application Directory"
   },
   AUTH: {
      en: "Auth"
   },
   DATA_PATH: {
      en: "Data Directory"
   },
   DETECTED_CONFIG: {
      en: "Detected Configuration"
   },
   LICENSE: {
      en: "License"
   },
   SECURITY: {
      en: "Security"
   },
   SERVER_OPERATING_SYSTEM: {
      en: "Server OS"
   },
   STORAGE: {
      en: "Storage"
   },
   TRIAL: {
      en: "Trial"
   },
   WELCOME: {
      en: "Welcome to the Toba Platform"
   }
} as PhraseList);