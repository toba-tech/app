import "./style.scss";
import * as React from "react";
import { RouteList } from "jsx/router";
import * as ReactDOM from "react-dom";
import Overview from "./overview";
import App from "jsx/app";

const routes:RouteList = { home: Overview };

ReactDOM.render(<App routes={routes} worksOffline={false}/>, document.getElementById("react-root"));