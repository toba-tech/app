import { Action, ActionType } from "modules/actions";
import { call } from "lib/web/socket/client";
import { StateStore, flux } from "lib/state/hub";
import { OperatingSystem } from "lib/host/os";

/**
 * Shape should match `ConfigurationResponse` in module.go.
 */
export interface SetupState {
   appPath:string;
   dataPath:string;
   processUser:string;
   licensed:boolean;
   hasSSL:boolean;
   os:OperatingSystem;
   selectedTab:number;
}

/**
 * State of the setup or trail.
 */
class SetupStore extends StateStore<SetupState> {
   constructor() {
      super({
         appPath: null,
         dataPath: null,
         processUser: null,
         licensed: false,
         os: OperatingSystem.Unknown,
         hasSSL: false,
         selectedTab: 0
      });
   }

   handler(action:ActionType, data?:any) {
      switch (action) {
         case Action.GetHostConfiguration:
            call(action, data)
               .then((config:any) => { this.update(config); })
               .catch(this.error("Unable to rerieve configuration").bind(this));

            break;
      }
   }
}

export const setupStore = flux.subscribe(new SetupStore());