import { Service } from "./services";
export { Status } from "lib/module/status";

/**
 * Client actions between components and stores as opposed to `Services` which
 * are server code invoked by web service calls.
 */
enum ClientAction {
   /** Send message to router component to update view. */
   ChangeView = 1,
   Login,
   ShowMenu,
   ShowWarning,
   SelectedAuthProvider
}

/**
 * Combined server and client actions. Merging enumerations requires jumping
 * through some hoops.
 * https://stackoverflow.com/documentation/typescript/4954/enums/27860/extending-enums-without-custom-enum-implementation#t=201701310857485274759
 */
export type ActionType = ClientAction | Service;
export const Action = {...Service, ...ClientAction };
export default Action;