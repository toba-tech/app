import { text } from '../localize';
import Localize from 'react-localization';
export default new Localize(Object.assign({}, text, {
   en: {
      app_name: 'Forums'
   }
}));