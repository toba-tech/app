import './style.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import log from '../worker';
import Header from '../components/header';
import Footer from '../components/footer';

class SystemApp extends React.Component {
   componentDidMount() { log.appStart('SystemApp'); }
   componentWillUnmount() { log.appExit('SystemApp'); }
   render() {
      return <div>
         <Header/>
         <Footer/>
      </div>;
   }
}

ReactDOM.render(<SystemApp/>, document.getElementById('react-root'));