package maker

import (
	"time"
)

type (
	Product struct {
		Name        string
		Description string
		CreatedAt   time.Time
	}

	ProductQuery struct {
		Product
		PartialName   string
		CreatedBefore time.Time
		CreatedAfter  time.Time
	}
)
