package maker

import (
	"toba.tech/app/lib/module"
	"toba.tech/app/modules"
)

const (
	AddMaterial module.ServiceID = iota + modules.Maker
	AddInventory
)

// var store = &module.Storage{
// 	Schemas: []*db.SqlTable{
// 		materialTable,
// 		inventoryTable,
// 	},
// }

// var Module = module.ModuleInfo{
// 	ID:     modules.Maker,
// 	Path:   "maker",
// 	Stores: []*module.Storage{store},
// 	Services: module.ServiceMap{
// 		AddMaterial: &module.Endpoint{
// 			PayloadType: func() interface{} {
// 				return &material{}
// 			},
// 			Service: SaveMaterial,
// 		},
// 		AddInventory: &module.Endpoint{
// 			PayloadType: func() interface{} {
// 				return &inventory{}
// 			},
// 			Service: SaveInventory,
// 		},
// 	},
// }
