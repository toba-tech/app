package maker

type (
	inventory struct {
		MaterialID int64
		Quantity   int
	}
)

// var inventoryTable = &db.SqlTable{
// 	Name: "inventory",
// 	Definition: `
// id			INT NOT NULL
// quantity INT NOT NULL
// 	`,
// }

// // SaveInventory creates an inventory record.
// func SaveInventory(ctx *module.Context) *module.Response {
// 	i := ctx.Payload.(inventory)
// 	return store.Insert(ctx, inventoryTable, db.SqlParameters{
// 		"id":       i.MaterialID,
// 		"quantity": i.Quantity,
// 	})
// }
