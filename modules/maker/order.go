package maker

type (
	order struct {
		MaterialID int64
		Quantity   int
	}
)

// var orderTable = &db.SqlTable{
// 	Name: "order",
// 	Definition: `
// placedOn 	DATETIME NOT NULL
// status 		INT NOT NULL
// customerID 	INT
// 	`,
// }

// // SaveOrder creates an order record.
// func SaveOrder(ctx *module.Context) *module.Response {
// 	i := ctx.Payload.(inventory)
// 	return store.Insert(ctx, inventoryTable, db.SqlParameters{
// 		"id":       i.MaterialID,
// 		"quantity": i.Quantity,
// 	})
// }

// func UpdateOrderStatus(ctx *module.Context) *module.Response {
// 	return module.Success(nil)
// }
