CREATE TABLE materials (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT NOT NULL,
   unit_type_id INT
);
ALTER TABLE materials ADD FOREIGN KEY (unit_type_id) REFERENCES unit_types (id);

CREATE TABLE products (
   id SERIAL PRIMARY KEY NOT NULL,
   name TEXT,
   properties JSONB
);

CREATE TABLE orders (
   id SERIAL PRIMARY KEY NOT NULL,
   placed_on TIMESTAMPTZ NOT NULL,
   customer_id INT,
   status_id INT NOT NULL
);
ALTER TABLE orders ADD FOREIGN KEY (status_id) REFERENCES status (id);

CREATE TABLE order_products (
   order_id INT NOT NULL,
   product_id INT NOT NULL,
   quantity INT NOT NULL,
   CONSTRAINT order_products_pk PRIMARY KEY (order_id, product_id)
);
ALTER TABLE order_products ADD FOREIGN KEY (order_id) REFERENCES orders (id);
ALTER TABLE order_products ADD FOREIGN KEY (product_id) REFERENCES products (id);

CREATE TABLE product_materials (
   product_id INT NOT NULL,
   material_id INT NOT NULL,
   quantity INT NOT NULL,
   CONSTRAINT product_materials_pk PRIMARY KEY (product_id, material_id)
);
ALTER TABLE product_materials ADD FOREIGN KEY (product_id) REFERENCES products (id);
ALTER TABLE product_materials ADD FOREIGN KEY (material_id) REFERENCES materials (id);