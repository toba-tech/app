package maker

type (
	material struct {
		Name     string
		UnitType int
	}
)

// var materialTable = &db.SqlTable{
// 	Name: "material",
// 	Definition: `
// name		TEXT NOT NULL,
// unitType INT NOT NULL
// 	`,
// }

// // SaveMaterial creates a new material record and returns its ID.
// func SaveMaterial(ctx *module.Context) *module.Response {
// 	m := ctx.Payload.(material)
// 	return store.Insert(ctx, materialTable, db.SqlParameters{
// 		"name":     m.Name,
// 		"unitType": m.UnitType,
// 	})
// }
