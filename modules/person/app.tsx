import "./style.scss";
import * as React from "react";
import { RouteList } from "jsx/router";
import * as ReactDOM from "react-dom";
import Directory from "./directory";
import App from "jsx/app";

const routes:RouteList = {
   home: Directory,
   views: []
};

ReactDOM.render(<App routes={routes} worksOffline={false}/>, document.getElementById("react-root"));