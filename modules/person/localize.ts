import { translate, PhraseList, Translation } from "lib/i18n/localize";

interface Phrases extends Translation {
   APP_NAME:string;
}

export default translate<Phrases>({
   APP_NAME: {
      en: "Person"
   }
} as PhraseList);