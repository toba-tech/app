import * as React from "react";
import Logo from "jsx/logo";
import FluxComponent from "jsx/flux";
import Action from "modules/actions";
import { personStore, PersonState } from "./store";

export default class extends FluxComponent<{}, PersonState> {
   /** Number of selected authentication providers. */
   selectionCount = 0;

   constructor(props:any) {
      super(props, personStore);
      this.emit(Action.GetUser, "user1");
   }

   render() {
      return <div id="person">
         <Logo size={100}/>
      </div>;
   }
}