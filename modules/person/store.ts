import { Action, ActionType } from "modules/actions";
import { call } from "lib/web/socket/client";
import { StateStore, flux } from "lib/state/hub";

export interface PersonState {
   name:string;
}

/**
 * State of a person.
 */
class PersonStore extends StateStore<PersonState> {
   constructor() {
      super({
         name: null
      });
   }

   handler(action:ActionType, data?:any) {
      switch (action) {
         case Action.GetUser:
            call(action, data)
               .then((entry:any) => {
                  console.log("entry", entry);
               })
               .catch(this.error("Unable to rerieve account for " + data).bind(this));

            break;
      }
   }
}

export const personStore = flux.subscribe(new PersonStore());