package person

import (
	"log"

	"toba.tech/app/lib/ldap"
	"toba.tech/app/lib/module"
	"toba.tech/app/modules"
)

const (
	GetUser module.ServiceID = iota + modules.Person
)

var (
	text string
	// Module defines the services and storage used by the person module.
	Module = module.
		Register(modules.Person, "person").
		RequireLicense(false).
		Add(GetUser, module.MakeEndpoint(getUser, &text))
)

// getUser retrieves a user entry from Active Directory.
func getUser(req *module.Request) *module.Response {
	accountName := req.Payload.(*string)
	entry, err := ldap.FindUser(*accountName)
	if err != nil {
		log.Print(err)
		return module.Error(module.LdapError)
	}
	return module.Success(entry)
}
