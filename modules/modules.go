// Package modules assigns an identity to each module for the sake of
// licensing and for use as the iota base number of each module's
// service identifiers.
//
// Count begins at 500 to leave room for client action enumerations.
package modules

const (
	System = 500 * (iota + 1)
	Register
	Communications
	ConfigurationItems
	KnowledgeBase
	Maker
	Payroll
	IncidentManagement
	Security
	ServiceDesk
	Setup
	Forum
	EmployeeDirectory
	Blog
	Contracts
	Performance
	Vendor
	Person
	Cogs
)
