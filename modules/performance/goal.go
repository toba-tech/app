package performance

import "time"

type (
	Goal struct{ due time.Time }
)
