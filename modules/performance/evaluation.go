package performance

import "time"

type (
	Evaluation struct{ due time.Time }
)
