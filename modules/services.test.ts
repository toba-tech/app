/* tslint:disable:no-unused-expression */
import "mocha";
import { expect } from "chai";
import { Action } from "./actions";

describe("Actions", () => {
   it("are generated", () => {
      expect(Action.Login).to.exist;
   });
});