// Generated by /cmd/generate/main.go

/* tslint:disable:trailing-comma */
export enum Service {
    AddCredentials = 500,
    AddInventory = 3001,
    AddMaterial = 3000,
    AddServerLog = 501,
    GetHostConfiguration = 5500,
    GetUser = 9000,
}
/* tslint:enable:trailing-comma */
