package cogs

import "toba.tech/app/lib/db/schema"

var (
	// cellsTable defines the spreadsheet type of cog.
	cellsTable = schema.Create("cells").Columns(
		schema.RowIdColumn("cogID").Ref(cogTable, "id"),
		schema.StatusColumn("statusID"),
	)
)
