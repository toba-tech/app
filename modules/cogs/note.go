package cogs

import "toba.tech/app/lib/db/schema"

var (
	noteTable = schema.Create("note").Columns(
		schema.RowIdColumn("cogID").Ref(cogTable, "id"),
		schema.StatusColumn("statusID"),
	)
)
