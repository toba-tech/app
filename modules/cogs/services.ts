/*eslint-disable block-scoped-var, no-redeclare, no-control-regex, no-prototype-builtins*/
import * as protobuf from "protobufjs/minimal";

// Common aliases
const $Reader = protobuf.Reader;
const $Writer = protobuf.Writer;
const $util = protobuf.util;

// Exported root namespace
const $root = protobuf.roots["default"] || (protobuf.roots["default"] = {});

export class GetHostConfiguration {
   constructor(rpcImpl:protobuf.RPCImpl, requestDelimited:boolean, responseDelimited:boolean) {
      protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
   }

   getConfig(request:IConfigRequest, callback) {
      return this.rpcCall(getConfig, $root.ConfigRequest, $root.ConfigReply, request, callback);
   }
}

export const GetHostConfiguration2 = $root.GetHostConfiguration = (() => {

    /**
     * Constructs a new GetHostConfiguration service.
     * @exports GetHostConfiguration
     * @classdesc Represents a GetHostConfiguration
     * @extends $protobuf.rpc.Service

     */
    function GetHostConfiguration(rpcImpl:protobuf.RPCImpl, requestDelimited:boolean, responseDelimited:boolean) {
        protobuf.rpc.Service.call(this, rpcImpl, requestDelimited, responseDelimited);
    }

    (GetHostConfiguration.prototype = Object.create(protobuf.rpc.Service.prototype)).constructor = GetHostConfiguration;

    /**
     * Callback as used by {@link GetHostConfiguration#getConfig}.
     * @memberof GetHostConfiguration
     * @typedef GetConfigCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {ConfigReply} [response] ConfigReply
     */

    /**
     * Calls GetConfig.
     * @function .getConfig
     * @memberof GetHostConfiguration
     * @instance
     * @param {IConfigRequest} request ConfigRequest message or plain object
     * @param {GetHostConfiguration.GetConfigCallback} callback Node-style callback called with the error, if any, and ConfigReply
     * @returns {undefined}
     * @variation 1
     */
    GetHostConfiguration.prototype.getConfig = function getConfig(request, callback) {
        return this.rpcCall(getConfig, $root.ConfigRequest, $root.ConfigReply, request, callback);
    };

    /**
     * Calls GetConfig.
     * @function getConfig
     * @memberof GetHostConfiguration
     * @instance
     * @param {IConfigRequest} request ConfigRequest message or plain object
     * @returns {Promise<ConfigReply>} Promise
     * @variation 2
     */

    return GetHostConfiguration;
})();

export const ConfigRequest = $root.ConfigRequest = (() => {

    /**
     * Properties of a ConfigRequest.
     * @exports IConfigRequest
     * @interface IConfigRequest
     * @property {string} [name] ConfigRequest name
     */

    /**
     * Constructs a new ConfigRequest.
     * @exports ConfigRequest
     * @classdesc Represents a ConfigRequest.
     * @constructor
     * @param {IConfigRequest=} [properties] Properties to set
     */
    function ConfigRequest(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ConfigRequest name.
     * @member {string}name
     * @memberof ConfigRequest
     * @instance
     */
    ConfigRequest.prototype.name = "";

    /**
     * Encodes the specified ConfigRequest message. Does not implicitly {@link ConfigRequest.verify|verify} messages.
     * @function encode
     * @memberof ConfigRequest
     * @static
     * @param {IConfigRequest} message ConfigRequest message or plain object to encode
     * @param {protobuf.Writer} [writer] Writer to encode to
     * @returns {protobuf.Writer} Writer
     */
    ConfigRequest.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.name != null && message.hasOwnProperty("name"))
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.name);
        return writer;
    };

    /**
     * Encodes the specified ConfigRequest message, length delimited. Does not implicitly {@link ConfigRequest.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ConfigRequest
     * @static
     * @param {IConfigRequest} message ConfigRequest message or plain object to encode
     * @param {protobuf.Writer} [writer] Writer to encode to
     * @returns {protobuf.Writer} Writer
     */
    ConfigRequest.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ConfigRequest message from the specified reader or buffer.
     * @function decode
     * @memberof ConfigRequest
     * @static
     * @param {protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ConfigRequest} ConfigRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ConfigRequest.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        const end = length === undefined ? reader.len : reader.pos + length, message = new $root.ConfigRequest();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.name = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a ConfigRequest message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ConfigRequest
     * @static
     * @param {protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ConfigRequest} ConfigRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ConfigRequest.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ConfigRequest message.
     * @function verify
     * @memberof ConfigRequest
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ConfigRequest.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.name != null && message.hasOwnProperty("name"))
            if (!$util.isString(message.name))
                return "name: string expected";
        return null;
    };

    /**
     * Creates a ConfigRequest message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ConfigRequest
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ConfigRequest} ConfigRequest
     */
    ConfigRequest.fromObject = function fromObject(object) {
        if (object instanceof $root.ConfigRequest)
            return object;
        const message = new $root.ConfigRequest();
        if (object.name != null)
            message.name = String(object.name);
        return message;
    };

    /**
     * Creates a plain object from a ConfigRequest message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ConfigRequest
     * @static
     * @param {ConfigRequest} message ConfigRequest
     * @param {protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ConfigRequest.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        const object = {};
        if (options.defaults)
            object.name = "";
        if (message.name != null && message.hasOwnProperty("name"))
            object.name = message.name;
        return object;
    };

    /**
     * Converts this ConfigRequest to JSON.
     * @function toJSON
     * @memberof ConfigRequest
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ConfigRequest.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, protobuf.util.toJSONOptions);
    };

    return ConfigRequest;
})();

export const ConfigReply = $root.ConfigReply = (() => {

    /**
     * Properties of a ConfigReply.
     * @exports IConfigReply
     * @interface IConfigReply
     * @property {string} [message] ConfigReply message
     */

    /**
     * Constructs a new ConfigReply.
     * @exports ConfigReply
     * @classdesc Represents a ConfigReply.
     * @constructor
     * @param {IConfigReply=} [properties] Properties to set
     */
    function ConfigReply(properties) {
        if (properties)
            for (let keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ConfigReply message.
     * @member {string}message
     * @memberof ConfigReply
     * @instance
     */
    ConfigReply.prototype.message = "";

    /**
     * Encodes the specified ConfigReply message. Does not implicitly {@link ConfigReply.verify|verify} messages.
     * @function encode
     * @memberof ConfigReply
     * @static
     * @param {IConfigReply} message ConfigReply message or plain object to encode
     * @param {protobuf.Writer} [writer] Writer to encode to
     * @returns {protobuf.Writer} Writer
     */
    ConfigReply.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        if (message.message != null && message.hasOwnProperty("message"))
            writer.uint32(/* id 1, wireType 2 =*/10).string(message.message);
        return writer;
    };

    /**
     * Encodes the specified ConfigReply message, length delimited. Does not implicitly {@link ConfigReply.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ConfigReply
     * @static
     * @param {IConfigReply} message ConfigReply message or plain object to encode
     * @param {protobuf.Writer} [writer] Writer to encode to
     * @returns {protobuf.Writer} Writer
     */
    ConfigReply.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ConfigReply message from the specified reader or buffer.
     * @function decode
     * @memberof ConfigReply
     * @static
     * @param {protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ConfigReply} ConfigReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ConfigReply.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        const end = length === undefined ? reader.len : reader.pos + length, message = new $root.ConfigReply();
        while (reader.pos < end) {
            const tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.message = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        return message;
    };

    /**
     * Decodes a ConfigReply message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ConfigReply
     * @static
     * @param {protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ConfigReply} ConfigReply
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ConfigReply.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ConfigReply message.
     * @function verify
     * @memberof ConfigReply
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ConfigReply.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (message.message != null && message.hasOwnProperty("message"))
            if (!$util.isString(message.message))
                return "message: string expected";
        return null;
    };

    /**
     * Creates a ConfigReply message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ConfigReply
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ConfigReply} ConfigReply
     */
    ConfigReply.fromObject = function fromObject(object) {
        if (object instanceof $root.ConfigReply)
            return object;
        const message = new $root.ConfigReply();
        if (object.message != null)
            message.message = String(object.message);
        return message;
    };

    /**
     * Creates a plain object from a ConfigReply message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ConfigReply
     * @static
     * @param {ConfigReply} message ConfigReply
     * @param {protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ConfigReply.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        const object = {};
        if (options.defaults)
            object.message = "";
        if (message.message != null && message.hasOwnProperty("message"))
            object.message = message.message;
        return object;
    };

    /**
     * Converts this ConfigReply to JSON.
     * @function toJSON
     * @memberof ConfigReply
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ConfigReply.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, protobuf.util.toJSONOptions);
    };

    return ConfigReply;
})();

export { $root as default };
