package cogs

import (
	"toba.tech/app/lib/module"
	"toba.tech/app/modules"
)

var (
	cogDB = module.Database("cogs",
		cogTable,
		labelTable,
		cogLabelTable,
		cogLinkTable,
		cellsTable,
		noteTable,
		imageTable,
		exifTable,
		commentTable,
		viewTable,
		participantTable,
		taskTable,
	)

	// Module defines the services and storage used by the tasks module.
	Module = module.
		Register(modules.Cogs, "cogs").
		RequireLicense(true).Storage(cogDB)
	//Add(GetHostConfiguration, module.MakeEndpoint(getConfiguration, nil))
)
