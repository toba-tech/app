package cogs

import "toba.tech/app/lib/db/schema"

type Access int

const (
	None Access = iota
	Read
	Change
	Owner
)

var (
	// polyTable defines a group of cogs, a "polycog".
	polyTable = schema.Create("poly").Columns(
		schema.RowIdColumn("id"),
		schema.TextColumn("name"),
	)

	// polyCogTable matches cogs to the poly view.
	polyCogTable = schema.Create("polyCog").Columns(
		schema.IntColumn("polyID").Ref(polyTable, "id").IsKey(),
		schema.IntColumn("cogID").Ref(cogTable, "id").IsKey(),
	)

	participantTable = schema.Create("participant").Columns(
		schema.IntColumn("polyID").Ref(polyTable, "id").IsKey(),
		schema.TextColumn("userID").IsKey(),
		schema.IntColumn("access"),
	)
)
