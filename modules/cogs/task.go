package cogs

import "toba.tech/app/lib/db/schema"

type RelationType int

const (
	Parent RelationType = iota
	DependsOn
)

var (
	// taskTable defines tasks.
	taskTable = schema.Create("task").Columns(
		schema.RowIdColumn("cogID").Ref(cogTable, "id"),
		schema.StatusColumn("statusID"),
		schema.TextColumn("filePath"),
	)

	taskRelationTable = schema.Create("taskRelation").Columns(
		schema.IntColumn("fromTaskID").Ref(taskTable, "cogID").IsKey(),
		schema.IntColumn("toTaskID").Ref(taskTable, "cogID").IsKey(),
		schema.IntColumn("type"),
	)

	criterionTable = schema.Create("criterion").Columns(
		schema.RowIdColumn("id"),
		schema.IntColumn("taskID").Ref(taskTable, "cogID"),
		schema.TextColumn("description"),
		schema.StatusColumn("status"),
	)
)
