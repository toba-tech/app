package cogs

import "toba.tech/app/lib/db/schema"

type CogType int

const (
	Cells CogType = iota
	Note
	Image
	Task
	Link
)

var (
	// cogTable defines the common elements of all cog types.
	cogTable = schema.Create("cog").Columns(
		schema.RowIdColumn("id"),
		schema.IntColumn("type"),
		schema.TimeStampColumn("created"),
		schema.StatusColumn("statusID"),
		schema.TextColumn("name"),
		schema.TextColumn("description"),
	)

	// tagTable defines the
	labelTable = schema.Create("label").Columns(
		schema.RowIdColumn("id"),
		schema.TextColumn("name"),
	)

	cogLabelTable = schema.Create("taskLabel").Columns(
		schema.IntColumn("cogID").Ref(cogTable, "id").IsKey(),
		schema.IntColumn("labelID").Ref(labelTable, "id").IsKey(),
	)

	cogLinkTable = schema.Create("taskLink").Columns(
		schema.IntColumn("cogID").Ref(cogTable, "id").IsKey(),
		schema.TextColumn("link").IsKey(),
	)

	commentTable = schema.Create("comment").Columns(
		schema.RowIdColumn("id"),
		schema.IntColumn("cogID").Ref(cogTable, "id"),
	)
)
