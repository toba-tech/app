package cogs

import "toba.tech/app/lib/db/schema"

var (
	// imageTable defines the image kind of cog.
	imageTable = schema.Create("image").Columns(
		schema.RowIdColumn("cogID").Ref(cogTable, "id"),
		schema.StatusColumn("statusID"),
		schema.TextColumn("filePath"),
	)

	exifTable = schema.Create("exif").Columns(
		schema.RowIdColumn("cogID").Ref(cogTable, "id"),
		schema.StatusColumn("statusID"),
		schema.TextColumn("filePath"),
	)
)
