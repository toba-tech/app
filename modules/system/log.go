package system

import (
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

// type logEntry struct {
// 	On time.Time
// }

var logTable = schema.Create("log").Columns(
	schema.TextColumn("subdomain").Unique().CaseInsensitive(),
)

// SaveLog adds a new log entry.
func SaveLog(req *module.Request) *module.Response {
	return logDB.ExecResponse(db.
		InsertInto(logTable, "id", "subdomain").
		Values("", ""))
}
