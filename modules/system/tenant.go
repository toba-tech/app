package system

import (
	"log"

	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/db/status"
	"toba.tech/app/lib/module"
)

type Credentials struct {
	UserName string
	Password string
}

// Tenant contains
var tenantTable = schema.Create("tenant").Columns(
	schema.RowIdColumn("id"),
	schema.UlidColumn("key"),
	schema.TimeStampColumn("created"),
	schema.StatusColumn("statusID"),
	schema.TextColumn("subdomain").Unique().CaseInsensitive(),
)

// TenantExists checks whether a tenant exists having a given domain name. At
// the same time, any tenant records still in a pending state an hour after
// creation are removed.
func TenantExists(req *module.Request) *module.Response {
	var exists bool

	err := systemDB.Transact(func(tx *db.Transaction) error {
		_, err := db.
			DeleteFrom(tenantTable).
			Where("statusID = ? AND created < datetime('now','-1 hour')", int(status.Pending)).
			Run(tx)

		if err != nil {
			log.Printf("Unable to delete tenant reservation: %v", err)
			return err
		}

		exists, err = db.
			ExistsIn(tenantTable).
			Where("subdomain = ?", req.Payload).
			Run(tx)

		return err
	})

	if err != nil {
		return module.Error(module.DatabaseError)
	}
	return module.Success(exists)
}

// TenantIdForSubdomain gets the tenant ID having a subdomain.
func TenantIdForSubdomain(subdomain string) (int64, error) {
	var id int64
	err := systemDB.First(db.Select("id").From(tenantTable).Where("subdomain = ?", subdomain),
		&id,
	)
	if err != nil {
		return 0, err
	}
	return id, err
}

// TenantKeyForID gets the ULID key for a tenant for use in the data file path.
func TenantKeyForID(id int64) ([]byte, error) {
	var key []byte
	err := systemDB.First(db.Select("key").From(tenantTable).Where("id = ?", id),
		&key,
	)
	if err != nil {
		return nil, err
	}
	return key, err
}
