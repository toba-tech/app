// Package system
package system

import (
	"toba.tech/app/lib/module"
	"toba.tech/app/modules"
)

const (
	AddCredentials module.ServiceID = iota + modules.System
	AddServerLog
)

type AuthRequest struct {
	State   string `json:"authState"`
	BaseURL string `json:"baseURL"`
}

var (
	text string

	Module = module.
		Register(modules.System, "system").
		RequireLicense(false).
		HasWebClient(false).
		Storage(systemDB, logDB).
		Add(AddServerLog, module.
			MakeEndpoint(SaveLog, &Credentials{}).
			RequireLogin(false),
		)
)
