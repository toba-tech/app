package system

import (
	"toba.tech/app/lib/module"
)

var (
	systemDB = module.Database("system",
		userTable,
		licenseTable,
	)
	chatDB = module.Database("chat",
		chatMessageTable,
		chatGroupTable,
		chatMemberTable,
	)
	activityDB = module.Database("activity", activityTable)
	logDB      = module.Database("log", logTable)
)
