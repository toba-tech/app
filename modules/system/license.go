package system

import (
	"time"

	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

type license struct {
	CustomerID int64
	ModuleID   int
	BeginOn    time.Time
	EndOn      time.Time
}

var licenseTable = schema.Create("license").WithoutRowID().Columns(
	schema.IntColumn("customerID").IsKey(),
	schema.IntColumn("moduleID").IsKey(),
	schema.StatusColumn("statusID"),
	schema.DateColumn("beginOn"),
	schema.DateColumn("endOn"),
)

func SaveLicense(req *module.Request) *module.Response {
	l := req.Payload.(license)
	return systemDB.ExecResponse(db.
		InsertInto(licenseTable, "customerID", "moduleID", "beginOn", "endOn").
		Values(l.CustomerID, l.ModuleID, l.BeginOn, l.EndOn))
}
