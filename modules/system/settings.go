package system

import (
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

var settingsTable = schema.Create("settings").Columns(
	schema.RowIdColumn("version"),
	schema.IntColumn("updatedBy").Ref(userTable, "id"),
	schema.TimeStampColumn("updatedOn"),
	schema.BooleanColumn("allowChat"),
	schema.BooleanColumn("recordChat"),
	schema.IntColumn("keepChatLogDays").Default("0"),
)

func SaveSettings(req *module.Request) *module.Response {
	return systemDB.ExecResponse(db.
		InsertInto(activityTable, "when").
		Values(schema.CURRENT_TIMESTAMP))
}
