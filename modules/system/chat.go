package system

import (
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

var (
	chatMessageTable = schema.Create("message").Columns(
		schema.RowIdColumn("userID"),
		schema.IntColumn("groupID").Ref(chatGroupTable, "id"),
		schema.TextColumn("message"),
		schema.TimeStampColumn("sent"),
	)

	chatGroupTable = schema.Create("group").Columns(
		schema.RowIdColumn("id"),
		schema.TimeStampColumn("created"),
	)

	chatMemberTable = schema.Create("member").Columns(
		schema.IntColumn("userID").IsKey(),
		schema.IntColumn("groupID").IsKey().Ref(chatGroupTable, "id"),
		schema.TimeStampColumn("joined"),
		schema.DateColumn("left"),
	)
)

func SaveChat(req *module.Request) *module.Response {
	// return logStore.Insert(req.TenantID, logTable, db.SqlParameters{
	// 	"when": col.CURRENT_TIMESTAMP,
	// })
	return module.Success("")
}
