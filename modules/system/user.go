package system

import (
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

var userTable = schema.Create("user").Columns(
	schema.RowIdColumn("id"),
	schema.TextColumn("firstName"),
	schema.TextColumn("lastName"),
	schema.TimeStampColumn("createdOn"),
	schema.BooleanColumn("enabled"),
)

func SaveUser(req *module.Request) *module.Response {
	return systemDB.ExecResponse(db.
		InsertInto(activityTable, "when").
		Values(schema.CURRENT_TIMESTAMP))
}
