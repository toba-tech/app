# Development Environment
## Application
   1. Install Delve for debugging `go get github.com/derekparker/delve/cmd/dlv`
   2. At admin prompt: `npm install --global --production windows-build-tools`

### Suggested Tools:
- [Visual Studio Code](http://code.visualstudio.com/)

## Dependencies
- github.com/gorilla/websocket
- github.com/stretchr/testify
- github.com/oklog/ulid
- github.com/divan/gofresh
- golang.org/x/crypto/blake2b
- golang.org/x/oauth2
- cloud.google.com/go/compute/metadata (used by oauth2/google)
- gopkg.in/ldap.v2
- github.com/derekparker/delve/cmd/dlv
- github.com/mattn/go-sqlite3
- github.com/josephspurrier/goversioninfo/cmd/goversioninfo
- golang.org/x/sys/windows/svc/
- github.com/codegangsta/gin
- github.com/golang/protobuf/protoc-gen-go

## Icons
https://github.com/google/material-design-icons/releases

## SSL Certificates
### Windows
- `certmgr` adds user certificates
- `certlm` adds local machine certificates
-  Local Computer Policy > Computer Configuration > Windows Settings > Security Settings. Next Public Key Policies. Double-click Certificate Path Validation Settings, and then select the Stores tab.
http://www.thewindowsclub.com/manage-trusted-root-certificates-windows

Let's Encrypt for free:
https://blog.kowalczyk.info/article/Jl3G/https-for-free-in-go.html

### OSX
- Add with Key Chain and update Trust Settings to "Always Trust"

## SQLite in Windows
- Download and install `tdm64-gcc-5.1.0-2.exe` from http://tdm-gcc.tdragon.net/download.
- Go to Program Files and click on "MinGW Command Prompt". This will open a console with the correct environment for using MinGW with GCC.
- Within this console, navigate to your GOPATH.
- Enter the following commands:
```
go get -u github.com/mattn/go-sqlite3
go install github.com/mattn/go-sqlite3
```

## Active Directory
   1. Download [Windows Server 2008 VHD](https://www.microsoft.com/en-us/download/details.aspx?id=2227)
   1. Run self-extracting RAR
   1. Select `Actions\New\Virtual Machine` in the Hyper-V Manager
   1. Provide a name like "Domain Controller"
   1. Choose *Generation 1*
   1. Select `Default Switch` for the connection
      - Hardcode the host IPv4 address to 192.168.12.1 (or something)
      - Subnet 255.255.255.0
      - DNS and Gateway can remain blank
   1. Select the downloaded VHD as an "existing virtual hard disk"
   1. Launch machine and add Active Directory Services role
      - run `oobe` to show Configuration Tasks if needed
      - default credentials are `Administrator`/`pass@word1`
   1. Update IPv4 settings for virtual LAN
      - *IP address*: 192.168.12.10
      - *Subnet mask*:  255.255.255.0
      - *Default gateway*: 192.168.12.1 (or whatever matches host)
      - *DNS*: 192.168.12.1
   1. Run `dcpromo` ([note](http://stef.thewalter.net/how-to-create-active-directory-domain.html))
      - Choose Create a new domain in a new forest
      - Enter base domain
      - Choose the Forest functional leve
      - Choose Windows Server 2008 R2
      - Ensure DNS server is selected
      - Choose `Yes` when warned about delegation
      - Accept default file paths
   1. Open Active Directory Users and Computers and add logins
      - Optionally reduce password security by running `gpmc.msc`
      - Example user: `ldap`/`password` as Toba Service


https://technet.microsoft.com/en-us/library/ee256063(v=ws.10).aspx
https://www.petri.com/test-connectivity-to-an-active-directory-domain-controller-from-pc

### Suggested Tools:
- [Active Directory Explorer](https://technet.microsoft.com/en-us/sysinternals/adexplorer.aspx) (Windows)

# Hosting

