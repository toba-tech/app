package main

import (
	"log"
	"net/http"

	"fmt"

	"os"

	"strings"

	"toba.tech/app/lib/auth"
	"toba.tech/app/lib/config"
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/module"
	"toba.tech/app/lib/web"
	"toba.tech/app/lib/web/socket"
	"toba.tech/app/modules/system"
)

var (
	modules = []*module.ModuleInfo{
		system.Module,
		register.Module,
	}

	authProviders = []*auth.AuthProvider{
		auth.NewLdapProvider(),
		auth.NewDropboxProvider(),
		auth.NewFacebookProvider(),
		auth.NewGoogleProvider(),
	}
)

// main runs database migrations and initializes dependencies for the HTTP and
// web socket handlers. Web sockets handle all service calls defined by modules
// while module paths are made HTTP endpoints that load client React components.
func main() {
	c, err := config.Load()
	web.ExitIfError(err)
	web.ExitIfError(db.Initialize(c.Database, tenant.DataPath))
	web.ExitIfError(system.Module.Migrate(0))

	// used to enable HTTP handlers for OAuth2 callback URLs
	authPaths := make(map[string]*auth.AuthProvider)

	for _, p := range authProviders {
		if p.Enabled {
			prefix := "TOBA_" + strings.ToUpper(p.Key)
			secret := os.Getenv(prefix + "_SECRET")
			key := os.Getenv(prefix + "_KEY")

			if key == "" || secret == "" {
				log.Fatal(p.Key + " authentication cannot be enabled without key or secret")
			} else if strings.ContainsRune(key, '\n') || strings.ContainsRune(secret, '\n') {
				log.Fatal(p.Key + " client key or secret contains invalid characters")
			} else {
				p.Initialize(key, secret)
				authPaths["auth/"+p.Key] = p
			}
		}
	}

	web.ExitIfError(auth.Initialize(tenant.Login, authProviders...))

	services, modulePaths := module.Amalgamate(modules)
	serviceHandler := module.Handle(services)

	log.Printf("Initializing\n%d modules\n%d services\n%d authentication providers", len(modulePaths), len(services), len(authPaths))

	http.HandleFunc("/ws", socket.Handle(c.HTTP, serviceHandler, resolveTenant))
	http.HandleFunc("/", web.Handle(c.HTTP, modulePaths, authPaths))
	log.Printf("Server starting on port %d", c.HTTP.Port)
	addr := fmt.Sprintf(":%d", c.HTTP.Port)

	// Consider http://goroutines.com/ssl
	err = http.ListenAndServeTLS(addr, c.HTTP.SslCert, c.HTTP.SslKey, nil)

	if err != nil {
		log.Fatal(err)
	}
}

// resolveTenant matches the current URL host subdomain to a registered tenant
// and returns that tenant's ID. A result of 0 is an error since auto-increment
// IDs begin at 1.
//
// The match shouldn't be cached since a tenant can change their subdomain at
// any time.
func resolveTenant(r *http.Request) int64 {
	parts := strings.Split(r.URL.Hostname(), ".")
	if len(parts) < 3 {
		return 0
	}
	id, err := system.TenantIdForSubdomain(parts[0])
	if err != nil {
		return 0
	}
	return id
}
