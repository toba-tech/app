package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/module"
)

func TestServiceCombination(t *testing.T) {
	keys := make(map[module.ServiceID]bool)

	for _, m := range modules {
		for k, _ := range m.Services {
			assert.NotContains(t, keys, k)
			keys[k] = true
		}
	}
}
