// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// From https://golang.org/src/crypto/tls/generate_cert.go
package main

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"log"
	"math/big"
	"net"
	"os"
	"time"
)

const (
	keyFile  = "toba.key"
	crtFile  = "toba.crt"
	duration = 365 * 24 * time.Hour // year
	rsaBits  = 2048
)

var (
	names = []string{"toba.local", "*.toba.local", "localhost"}
	ips   = []net.IP{net.IPv4(127, 0, 0, 1)}
)

// openssl req -newkey rsa:2048 -x509 -nodes -keyout ./certs/toba.key -new -out ./certs/toba.crt
// -subj /CN=*.tobal.local -reqexts SAN -extensions SAN -config <(cat /System/Library/OpenSSL/openssl.cnf <(printf '[SAN]\nsubjectAltName=DNS:toba.local')) -sha256 -days 3650"
// https://codereview.appspot.com/12056043
func main() {
	notBefore := time.Now()
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)

	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	priv, err := rsa.GenerateKey(rand.Reader, rsaBits)
	if err != nil {
		log.Fatalf("failed to generate private key: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Toba"},
			CommonName:   "toba.tech",
			Locality:     []string{"Boise"},
			Province:     []string{"Idaho"},
			Country:      []string{"United States"},
			PostalCode:   []string{"83706"},
		},
		NotBefore:   notBefore,
		NotAfter:    notBefore.Add(duration),
		DNSNames:    names,
		IPAddresses: ips,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	certOut, err := os.Create("./certs/" + crtFile)
	if err != nil {
		log.Fatalf("failed to open "+crtFile+" for writing: %s", err)
	}
	pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	certOut.Close()
	log.Print("written " + crtFile + "\n")

	keyOut, err := os.OpenFile("./certs/"+keyFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Print("failed to open "+keyFile+" for writing:", err)
		return
	}
	pem.Encode(keyOut, pemBlockForKey(priv))
	keyOut.Close()
	log.Print("written " + keyFile + "\n")
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Unable to marshal ECDSA private key: %v", err)
			os.Exit(2)
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}
