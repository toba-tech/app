package main

import (
	"go/ast"
	"go/build"
	"go/importer"
	"go/parser"
	"go/token"
	"go/types"
	"log"
	"os"
	"path/filepath"
	"strings"

	exact "go/constant"
)

type (
	// Package contains information about all of a Go package's files and types.
	Package struct {
		// types found in every package file matched to an identifier.
		types map[*ast.Ident]types.Object
		// fileASTs contains the Abstract Syntax Trees (AST) for all package
		// files. The AST can be used to find all declarations of a given
		// type or any other Go syntax.
		fileASTs []*ast.File
	}

	// enumConfig indicates which Go constant should be converted to a
	// TypeScript enum and what files to scan and generate.
	enumConfig struct {
		// path is the Go source code path relative to the current working
		// directory in which to load Go files containing the constant type to be
		// converted.
		path string
		// typeName is the name of the Go `int` type used to define the `iota`
		// constants that should be expressed as TypeScript enumerations.
		typeName string
		// enumName is the TypeScript enum name that the constants will be
		// assigned to. If left empty, the constants will be assigned to an enum
		// having the name of their Go package.
		enumName string
		// outputFile is the path and name of the TypeScript file that will
		// contain the enum declaration.
		outputFile string
		// recurse indicates subdirectories of `path` should also be searched for
		// constants of type `typeName`.
		recurse bool
		// packages contains Go package information keyed to their name. The name
		// is used as the generated TypeScript enum name if `enumName` is empty.
		packages map[string]*Package
		// constants maps package names to constant names to their values. It
		// stores declarations as they're discovered, before being emitted to
		// TypeScript.
		constants map[string]map[string]int64
	}

	templateData struct {
		Modules map[string]int
		Actions map[string]int
	}
)

// parsePath finds all Go packages and files at a path to build package
// information with Abstract Syntax Trees and type definitions extracted
// for each package file.
func parsePath(c *enumConfig, path string) error {
	if path == "" {
		path = c.path
	}
	pkg, err := build.Default.ImportDir(path, 0)
	if err != nil {
		log.Printf("importing %s: %s", path, err)
	} else {
		names := prefixPath(path, pkg.GoFiles)

		if c.packages == nil {
			c.packages = make(map[string]*Package)
		}

		c.packages[strings.Title(pkg.Name)] = parsePackage(path, names)
	}

	if c.recurse {
		return filepath.Walk(path, func(foundPath string, f os.FileInfo, err error) error {
			if err == nil && f.IsDir() && foundPath != path {
				return parsePath(c, foundPath)
			}
			return nil
		})
	}
	return nil
}

// prefixPath adds the base path to the beginning of a list of file names to
// simplify subsequent reading and parsing.
func prefixPath(path string, names []string) []string {
	if path == "." {
		return names
	}
	ret := make([]string, len(names))
	for i, name := range names {
		ret[i] = filepath.Join(path, name)
	}
	return ret
}

// parsePackage builds package information for a list of files at a given path.
//
// Each file is parsed as an Abstract Syntax Tree (AST) which represents the
// file's source code as a navigable structure allowing discovery of constants
// and other types.
func parsePackage(path string, names []string) *Package {
	pkg := &Package{}
	fs := token.NewFileSet()

	for _, name := range names {
		if !strings.HasSuffix(name, ".go") {
			continue
		}
		parsedFile, err := parser.ParseFile(fs, name, nil, 0)
		if err != nil {
			log.Printf("parsing package: %s: %s", name, err)
			return nil
		}
		pkg.fileASTs = append(pkg.fileASTs, parsedFile)
	}
	if len(pkg.fileASTs) > 0 {
		pkg.types = typeDefinitions(path, fs, pkg.fileASTs)
		if pkg.types != nil {
			return pkg
		}
	}
	return nil
}

// typeDefinitions builds a list of types defined in each file AST using the
// `types` package to check and export types discovered in each file.
//
// The definition is a `types.Object` that gives the declaration name, type,
// whether it's public, its package and more.
func typeDefinitions(path string, fs *token.FileSet, astFiles []*ast.File) map[*ast.Ident]types.Object {
	defs := make(map[*ast.Ident]types.Object)
	config := types.Config{Importer: importer.Default(), FakeImportC: true}
	_, err := config.Check(path, fs, astFiles, &types.Info{Defs: defs})

	if err != nil {
		log.Printf("checking package: %s", err)
		return nil
	} else {
		return defs
	}
}

// constantValues finds the assigned values of constants and returns them
// mapped to the constant name.
func constantValues(pkg *Package, typeName string) map[string]int64 {
	constants := make(map[string]int64)
	for _, file := range pkg.fileASTs {
		if file != nil {
			ast.Inspect(file, inspectNode(pkg, typeName, constants))
		}
	}
	return constants
}

// inspectNode curries the function expected by `ast.Inspect` in order to
// capture constant definitions found at each AST node.
//
// The curried function simply returns true or false to indicate if
// `ast.Inspect` should also inspect children of the node. In this case, return
// false if the node is constant (doesn't have children) otherwise return true.
func inspectNode(pkg *Package, typeName string, constants map[string]int64) func(node ast.Node) bool {
	return func(node ast.Node) bool {
		// Cast the AST node as a declaration to reveal its type.
		decl, ok := node.(*ast.GenDecl)
		if !ok || decl.Tok != token.CONST {
			return true
		}
		// Declarations can contain multiple elements, such as in a parenthetical
		// list of constants or vars. Keep track of encountered type
		// speficiations that may apply to subsequent elements.
		foundType := ""

		// Each element in the declaration may indicate its type and value or
		// neither if part of an iota constant sequence.
		for _, spec := range decl.Specs {
			// Cast to ValueSpec will always succeed after filtering to
			// token.CONST.
			vspec := spec.(*ast.ValueSpec)
			if vspec.Type == nil && len(vspec.Values) > 0 {
				// No type or value given. For example, "X = 1".
				foundType = ""
				continue
			}
			if vspec.Type != nil {
				// Record type if it can be identified.
				ident, ok := vspec.Type.(*ast.Ident)
				if !ok {
					expr, ok := vspec.Type.(*ast.SelectorExpr)
					if !ok {
						continue
					}
					ident = expr.Sel
				}
				foundType = ident.Name
			}
			if foundType != typeName {
				continue
			}
			// Iterate over constant or variable names of the declared type.
			for _, name := range vspec.Names {
				if name.Name == "_" {
					continue
				}
				// This dance lets the type checker find the values for us. It's a
				// bit tricky: look up the object declared by the name, find its
				// types.Const, and extract its value.
				obj, ok := pkg.types[name]
				if !ok {
					log.Fatalf("no value for constant %s", name)
				}
				info := obj.Type().Underlying().(*types.Basic).Info()
				if info&types.IsInteger == 0 {
					log.Fatalf("can't handle non-integer constant type %s", foundType)
				}
				value := obj.(*types.Const).Val() // Guaranteed to succeed as this is CONST.
				if value.Kind() != exact.Int {
					log.Fatalf("can't happen: constant is not an integer %s", name)
				}
				i64, isInt := exact.Int64Val(value)
				u64, isUint := exact.Uint64Val(value)
				if !isInt && !isUint {
					log.Fatalf("internal error: value of %s is not an integer: %s", name, value.String())
				}
				if !isInt {
					i64 = int64(u64)
				}
				constants[name.Name] = i64
			}
		}
		return false
	}
}
