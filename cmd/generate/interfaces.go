package main

// File holds a single parsed file and associated data.
type (
	// enumConfig indicates which Go constant should be converted to a
	// TypeScript enum and what files to scan and generate.
	interfaceConfig struct {
		source     interface{}
		outputFile string
	}
)
