import "./warning.scss";
import * as React from "react";
import { is } from "lib/utility";
import Icon from "./icon";
import StateComponent from "./stateful";
import { Action, ActionType } from "modules/actions";

interface Props {
   hideAfterSeconds:number;
   defaultIconName:string;
}

interface State {
   message:string[];
   /** Material UI icon name */
   iconName?:string;
}

/**
 * Display system-wide warning.
 */
export default class extends StateComponent<Props, State> {
   constructor(props:any) {
      super(props, {
         message: null,
         iconName: null
      });
   }

   handler(action:ActionType, data:State) {
      if (action == Action.ShowWarning) {
         if (is.empty(data.iconName)) {
            data.iconName = this.props.defaultIconName;
         }
         this.setState(data);

         if (this.props.hideAfterSeconds > 0) {
            window.setTimeout(this.hide, this.props.hideAfterSeconds * 1000);
         }
      }
   }

   /**
    * Hide warning by nullifying the message.
    */
   hide() {
      this.setState({ message: null });
   }

   render() {
      return (is.empty(this.state.message)) ? null : (
         <div className="system-warning">
            <Icon name={this.state.iconName}/>
            <p>{ this.state.message }</p>
            <Icon onClick={this.hide.bind(this)} name="close" className="close"/>
         </div>
      );
   }
}