import "./footer.scss";
import * as React from "react";

interface Props {
   className?:string;
}

export default (props:Props)=> (
   <footer className={props.className}>
      Footer
   </footer>
);