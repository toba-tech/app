/* tslint:disable:no-unused-expression */
import * as mocha from "mocha";
import { render } from "./test-util";
import Logo from "./logo";

const cssSize = (size:number) => `width: ${size}px; height: ${size}px;`;

describe("Logo Component", () => {
   it("renders IMG tag at given size", () => {
      render(Logo, { size: 90 }).expect({
         tagName: "IMG",
         className: "logo",
         style: cssSize(90)
      });
   });

   it("renders IMG tag at default size", () => {
      render(Logo).expect({ style: cssSize(100) });
   });
});