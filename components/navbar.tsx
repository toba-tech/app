import * as React from "react";
import { Item as MenuItem } from "./menu";
import Notification from "./notifications";
import AccountMenu from "./account-menu";

interface Props {
   menu?:MenuItem[];
   activeMenu:string;
}

export default class extends React.Component<undefined, Props> {
   token:string;

   constructor() {
      super();
      this.state = {
         activeMenu: null
      };
   }

   componentDidMount() {
      // this.token = user.onChange(()=> {
      //    this.setState({
      //       activeMenu: 'Company'
      //    });
      // });
   }

   componentWillUnmount() {
      //user.ignore(this.token);
   }

   render() {
      return (
         <nav className="top-nav">
            <a href="/" className="logo"></a>
            <Notification/>
            <AccountMenu/>
         </nav>
      );
   }
}

// <Menu data={this.props.menu} show={this.state.activeMenu}/>
