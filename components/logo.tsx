import * as React from "react";
import SVG from "./svg";

interface Props { size?:number; }

export const Logo = (props:Props) => {
   const size:number = props.size ? props.size : 100;
   return <SVG
      className="logo"
      width={size}
      height={size}
      src="/img/logo.svg"/>;
};

export default Logo;