import "./unsupported.scss";
import * as React from "react";
import Icon from "./icon";
import { is } from "lib/utility";
import text from "lib/i18n/localize";

declare global {
   interface Window {
      WebSocket:WebSocket;
   }
}

interface Browser {
   key:string;
   name:string;
   url:string;
   version:number;
}

/**
 * Minimum browser versions with required features. Trademark names aren't
 * translated (should they be?).
 */
const minimums:Browser[] = [{
      key: "chrome",
      name: "Chrome",
      url: "https://www.google.com/chrome/browser/features.html",
      version: 49
   }, {
      key: "chrome-android",
      name: "Mobile Android",
      url: "https://play.google.com/store/apps/details?id=com.android.chrome",
      version: 59
   }, {
      key: "ie",
      name: "Internet Explorer",
      url: "https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads",
      version: 11
   }, {
      key: "edge",
      name: "Edge",
      url: "https://www.microsoft.com/en-us/windows/microsoft-edge",
      version: 14
   }, {
      key: "firefox",
      name: "Firefox",
      url: "https://www.mozilla.org/en-US/firefox/new/",
      version: 52
   }, {
      key: "opera",
      name: "Opera",
      url: "http://www.opera.com/download",
      version: 45
   }, {
      key: "safari",
      name: "Safari",
      url: "https://support.apple.com/downloads/safari",
      version: 10.1
   },
   {
      key: "safari-ios",
      name: "Mobile Safari",
      url: "",
      version: 9.3
   }
];

/**
 * Component to inform of unsupported browser.
 * https://github.com/alrra/browser-logos
 */
export default ()=> supportedBrowser() ? null
   : <div id="unsupported">
      <Icon name="warning"/>
      <p>{ text.UNSUPPORTED_BROWSER }</p>
   </div>;

/**
 * Whether browser supports required features. React 16 requires support for
 * `Map` and `Set`: https://github.com/facebook/react/issues/10294
 *
 * CSS Flex layout also has requirements, though they're surprassed by these
 * evaluations: https://www.w3schools.com/cssref/css3_pr_flex.asp
 *
 * http://caniuse.com/#feat=websockets
 * http://caniuse.com/#search=pushstate
 * http://caniuse.com/#search=webworker
 */
const supportedBrowser = ()=>
   window.WebSocket &&
   history.pushState &&
   window.indexedDB &&
   typeof(Map) === is.type.FUNCTION &&
   typeof(Set) === is.type.FUNCTION &&
   typeof(Worker) !== is.type.UNDEFINED &&
   typeof(Storage) !== is.type.UNDEFINED;