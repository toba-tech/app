import * as React from "react";
import Icon from "./icon";
import { flux } from "lib/state/hub";
import { Action } from "modules/actions";

export interface Item {
   title:string;
   items:Item[];
}

interface Props {
   data:Item[];
}

interface State {
   /** Index of menu item to show */
   show:number;
}

export class Menu extends React.Component<Props, State> {
   render() {
      return (
         renderItems(this.props.data, this.state.show)
      );
   }
}

function renderItems(items:Item[], visibleMenu?:number):React.ReactElement<any> {
   const root = (visibleMenu !== undefined);
   const underline = (root) ? <div className="underline"></div> : null;
   return (
      <ul className={(root) ? "menu" : "sub-menu"}>{
         items.map((item, i) => {
            if (root) {
               if (visibleMenu == i) {
                  return (
                     <li key={item.title}>
                        {item.title}
                        <Icon name="angle-down"/>
                        {underline}
                        {renderItems(item.items)}
                     </li>
                  );
               } else {
                  return (
                     <li onClick={onClick} key={item.title}>
                        {item.title}
                        <Icon name="angle-down"/>
                        {underline}
                     </li>
                  );
               }
            } else {
               return <li key={item.title}>{item.title}</li>;
            }
         }
      )}
      </ul>
   );
}

function onClick(e:any) {
   flux.emit(Action.ShowMenu, name);
}
