import "./offline.scss";
import * as React from "react";
import {
   ConnectionState,
   addOfflineHandler,
   removeOfflineHandler,
   retryConnection } from "lib/web/socket/client";
import Icon from "jsx/icon";
import { default as text, format } from "lib/i18n/localize";

/**
 * Component that displays status of websocket connection with countdown to
 * automatic reconnection if offline or link to immediately retry connection.
 */
export default class extends React.Component<{}, ConnectionState> {
   timer = 0;

   constructor(props:any) {
      super(props);
      this.state = {
         offline: false,
         secondsUntilRetry: 0
      };
   }

   componentDidMount() { addOfflineHandler(this.onDisconnect.bind(this)); }
   componentWillUnmount() { removeOfflineHandler(this.onDisconnect.bind(this)); }

   /**
    * Handle updated disconnection state. Show countdown to next automatic
    * retry.
    */
   onDisconnect(s:ConnectionState) {
      window.clearInterval(this.timer);
      this.setState(s);

      this.timer = window.setInterval(()=> {
         s.secondsUntilRetry--;
         if (s.secondsUntilRetry > 0) {
            this.setState(s);
         } else {
            window.clearInterval(this.timer);
         }
      }, 1000);
   }

   /**
    * Immediately attempt to reconnect.
    */
   reconnect() {
      window.clearInterval(this.timer);
      retryConnection();
   }

   render() {
      if (!this.state.offline) { return null; }

      const howLong = this.state.secondsUntilRetry > 0
         ? format(text.RETRYING, this.state.secondsUntilRetry) + ". "
         : null;

      return <div className="offline-warning">
         <Icon name="sync_problem"/>
         <p>{ text.DISCONNECTED }. { howLong }</p>
         <a onClick={ this.reconnect.bind(this) }>{ text.RETRY }</a>
      </div>;
   }
}