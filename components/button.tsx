import "./button.scss";
import * as React from "react";
import Icon from "./icon";

interface Props {
   title:string;
   icon?:string;
   disabled?:boolean;
   className?:string;
   type?:string;
   onClick?:()=>void;
}

export const Button = (props:Props) => {
   const icon = props.icon ? <Icon name={props.icon}/> : null;

   return <button
      disabled={props.disabled}
      className={props.className}
      type={props.type}
      onClick={props.onClick}>{icon} {props.title}</button>;
};

export default Button;