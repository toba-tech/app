import "./text-input.scss";
import * as React from "react";
import Icon from "./icon";

interface Props {
   /** Whether input should receive focus when component loads. */
   focus?:boolean;
   hint?:string;
   width?:number;
   regex?:RegExp;
   prefix?:string;
   prefixIcon?:string;
   suffix?:string;
   onValidation?:(valid:boolean, name:string)=>void;
   onEnter?:()=>void;
}

interface State {
   valid:boolean;
}

export class TextInput extends React.Component<Props, State> {
   onValidation:(valid:boolean, name:string)=>void;
   input?:HTMLInputElement;

   constructor(props:Props) {
      super(props);
      this.state = { valid: true };
      this.input = null;
      this.onValidation = this.props.onValidation ? this.props.onValidation : ()=> { return; };
   }

   componentDidMount() { this.input.focus(); }

   value() { return this.input.value; }
   isValid() { return this.state.valid; }

   onKeyUp(e:KeyboardEvent) {
      if (e.charCode == 13) {
         if (this.state.valid && this.props.onEnter) { this.props.onEnter(); }
      } else if (this.props.regex) {
         const re = this.props.regex;
         const value = this.value();
         const valid = re.test(value);
         this.setState({ valid });
         this.props.onValidation(valid, value);
      }
   }

   render() {
      const hintClass = this.state.valid ? "input-tip" : "input-tip error";
      const icon = this.props.prefixIcon ? <Icon name={this.props.prefixIcon}/> : null;

      return <div className="text-input">
         <div>
            {this.props.prefix ? <div className="input-prefix">{icon}{this.props.prefix}</div> : null}
            <input
               ref={e => this.input = e}
               type="text"
               onKeyUp={this.onKeyUp.bind(this)}
               style={this.props.width ? { width: this.props.width + "em" } : null}
            />
            {this.props.suffix ? <div className="input-suffix">{this.props.suffix}</div> : null}
         </div>
         {this.props.hint ? <div className={hintClass}>{this.props.hint}</div> : null}
      </div>;
   }
}

export default TextInput;