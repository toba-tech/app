import "./header.scss";
import * as React from "react";
import Icon from "./icon";

interface Props {
   title:string;
}

export default class extends React.Component<Props, undefined> {
   render() {
      return <header className="main">
         <Icon className="menu" name="menu"/>
         <span className="title">{this.props.title}</span>
         <div className="logo"/>
         <Icon className="account" name="person"/>
         <Icon className="app" name="apps"/>
      </header>;
   }
}