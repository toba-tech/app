import * as React from "react";

interface Props {
   text:string;
   title?:string;
}

export default (props:Props) => {
   const titleBar = (props.title !== undefined) ? <h1>{props.title}</h1> : null;

   return (
      <div className="modal">
         {titleBar}
         <div className="body">{props.text}</div>
         <footer className="modal">
            <button onClick={onClose}>Close</button>
         </footer>
      </div>
   );
};

function onClose() {
   alert("close");
}

