import "./tabs.scss";
import * as React from "react";
import Icon from "jsx/icon";

export interface Tab {
   /** Name to show in tab. */
   title:string;
   /** Name of material icon. */
   icon:string;
   /** React component rendered for the tab. */
   view:React.ComponentClass<any> | React.StatelessComponent<any>;
}

interface State {
   /** Index of the active tab */
   selected:number;
}

interface Props {
   tabs:Tab[];
   selected?:number;
}

export class Tabs extends React.Component<Props, State> {
   constructor(props:Props) {
      super(props);
      this.state = { selected: props.selected ? props.selected : 0 };
   }

   onTabClick(index:number) {
      this.setState({ selected: index });
   }

   componentWillReceiveProps(nextProps:Props) {
      if (nextProps.selected != this.props.selected) {
         this.setState({ selected: nextProps.selected });
      }
   }

   render() {
      const t:Tab = this.props.tabs[this.state.selected];
      const handler = this.onTabClick.bind(this);

      return (
         <nav className="tabs">
            <ul>
            { this.props.tabs.map((t, i) => (
               <li onClick={()=> handler(i)} className={(i == this.state.selected) ? "selected" : null}>
                   <Icon name={t.icon}/>{t.title}
               </li>
            ))}
            </ul>
            <div className="active-tab"><t.view/></div>
         </nav>
      );
   }
}
