import * as React from "react";
import Button from "./button";
import Icon from "./icon";
import { flux } from "lib/state/hub";
import { userStore, UserState } from "lib/state/user";

interface State {
   active:boolean;
   user:UserState;
}

export default class AccountMenu extends React.Component<undefined, State> {
   constructor(props:any) {
      super(props);
      this.state = {
         active: false,
         user: userStore.load()
      };
   }

   componentDidMount() { userStore.subscribe(this.onChange.bind(this)); }
   componentWillUnmount() { userStore.remove(this.onChange); }
   onChange() { this.setState({ user: userStore.state }); }

   toggle() { this.setState({ active: !this.state.active }); }
   logout() {
      this.setState({ active: false });
      flux.emit(0);
   }

   render() {
      if (this.state.user.signedIn) {
         const menu = (this.state.active)
            ? (<nav>
                  <h4>{this.state.user.fullName}</h4>
                  <Button onClick={this.logout.bind(this)} icon="close" title="Logout"/>
               </nav>)
            : null;

         return (
            <div id="account-menu">
               <img onClick={this.toggle.bind(this)} src={this.state.user.photoURL}/>
               {menu}
            </div>
         );
      } else {
         return <Icon className="signed-out" name="user"/>;
      }
   }
}