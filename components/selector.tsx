import "./selector.scss";
import * as React from "react";
import Icon from "./icon";

interface Props {
   selected?:boolean;
   disabled?:boolean;
   className?:string;
   /**
    * Option icon to display when the control is not selected. The default is
    * a plus symbol.
    */
   unselectedIcon?:string;
   onClick?:(checked:boolean)=>void;
}

interface State {
   selected:boolean;
}

/**
 * Selector component displays
 */
export default class extends React.Component<Props, State> {
   baseCSS:string;
   unselectedIcon:string;

   constructor(props:Props) {
      super(props);

      this.baseCSS = (props.className ? props.className + " " : "") + "selector";
      this.unselectedIcon = props.unselectedIcon ? props.unselectedIcon : "add";
      this.state = {
         selected: (props.selected)
      };
   }

   onClick() {
      this.setState({ selected: !this.state.selected });
      if (this.props.onClick) {
         this.props.onClick(this.state.selected);
      }
   }

   render() {
      const css = this.state.selected ? this.baseCSS + " selected" : this.baseCSS;
      const iconName = this.state.selected ? "check" : this.unselectedIcon;
      return <div
         className={css}
         onClick={this.onClick.bind(this)}><Icon name={iconName}/></div>;
   }
}