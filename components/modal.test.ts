/* tslint:disable:no-unused-expression */
import "mocha";
import { expect } from "chai";
import { render, tag } from "./test-util";
import Modal from "./modal";

describe("Modal Component", () => {
   it("renders modal", () => {
      const el = render(Modal, { text: "Body Text" }).expect({
         tagName: tag.div,
         className: "modal"
      });
      let list = el.getElementsByClassName("body");

      expect(list).is.length(1);
      const body = list[0];
      expect(body.innerHTML).equals("Body Text");
      expect(body.tagName).equals(tag.div);

      list = el.getElementsByTagName("footer");

      expect(list).is.length(1);
      const footer = list[0];
      expect(footer.getAttribute("class")).equals("modal");
   });
});