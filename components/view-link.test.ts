/* tslint:disable:no-unused-expression */
import "mocha";
import { render, tag } from "./test-util";
import ViewLink from "./view-link";

describe("View Link Component", ()=> {
   it("shows a link", ()=> {
      render(ViewLink, { href: "link", title: "title" }).expect({
         tagName: tag.a,
         innerHTML: "title",
         attribute: { href: "/link" }
      });
   });
});