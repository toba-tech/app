/* tslint:disable:no-unused-expression */
import * as mocha from "mocha";
import { render, tag } from "./test-util";
import Link from "./link";

describe("Link Component", () => {
   it("renders link tag", () => {
      render(Link, { title: "Test Link" }).expect({
         tagName: tag.a,
         innerHTML: "Test Link"
      });
   });
});