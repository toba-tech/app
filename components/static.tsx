import { is } from "lib/utility";
import * as React from "react";
import "whatwg-fetch";

const cache:{[key:string]:Content} = {};
interface Content { __html:string; }
interface Props { name:string; cache:boolean; }
interface State { content:Content; }

/**
 * Display static server content.
 *
 * https://facebook.github.io/react/tips/dangerously-set-inner-html.html
 */
export default class Static extends React.Component<Props, State> {
   fileName:string;
   useCache:boolean;
   cacheHit:boolean;

   constructor(props:Props) {
      super(props);
      // pattern required by dangerouslySetInnerHTML
      let html = { __html: "" };

      this.fileName = this.props.name;
      this.useCache = this.props.cache;
      this.cacheHit = false;

      if (this.useCache && is.defined(cache, this.fileName)) {
         html = cache[this.fileName];
         this.cacheHit = true;
      }
      this.state = { content: html };
   }

   // https://github.com/github/fetch
   componentDidMount() {
      if (this.cacheHit) { return; }
      // load content from server if not already cached
      // fetch(config.partials.url + this.fileName + '.html')
      //    .then(r => r.text())
      //    .then(html => {
      //       this.setState({ content: html });
      //       if (this.useCache) { cache[this.fileName] = html; }
      //    });

      // http
      // 	.get(config.partials.url + this.fileName + '.html')
      // 	.end((err, res) => {
      // 		if (err === null) {
      // 			let html = { __html: res.text };
      // 			this.setState({ content: html });
      // 			if (this.useCache) { cache[this.fileName] = html; }
      // 		}
      // 	});
   }

   render() {
      return <div dangerouslySetInnerHTML={this.state.content}/>;
   }
}