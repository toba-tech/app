import * as React from "react";

interface Member {
   name:string;
}

interface Props {
   onClick:()=>void;
   member:Member;
}

export default (props:Props) => (
   <div className="card member" onClick={props.onClick}>
      { props.member.name }
   </div>
);