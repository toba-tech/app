/* tslint:disable:no-unused-expression */
import "mocha";
import { render, tag } from "./test-util";
import Button from "./button";

describe("Button Component", () => {
   it("renders button tag", () => {
      render(Button, { title: "Test Button" }).expect({
         tagName: tag.button,
         innerHTML: "Test Button"
      });
   });
});