/* tslint:disable:no-unused-expression */
import "mocha";
import { render, tag } from "./test-util";
import Icon from "./icon";

describe("Icon Component", () => {
   it("renders icon I tag", () => {
      render(Icon, { name: "some-icon" }).expect({
         tagName: tag.i,
         className: "material-icons",
         innerHTML: "some-icon"
      });
   });

   it("concatenates CSS class names", () => {
      render(Icon, { name: "some-icon", className: "another" }).expect({
         className: "material-icons another"
      });
   });
});