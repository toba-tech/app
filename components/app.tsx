import * as React from "react";
import Offline from "./offline";
import Unsupported from "./unsupported";
import { Router, RouteList } from "./router";

interface Props {
   worksOffline:boolean;
   routes:RouteList;
}

/**
 * Base app component encapsulates the router and websocket connectivity
 * management.
 */
export default (props:Props) => (
   <div className="main-view">
      <Offline/>
      <Unsupported/>
      <Router routes={props.routes}/>
   </div>
);