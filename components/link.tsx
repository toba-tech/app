import * as React from "react";
import Icon from "./icon";

interface Props {
   icon?:string;
   disabled:boolean;
   title:string;
   type?:string;
   className?:string;
   onClick?:()=>void;
}

export default (props:Props) => {
   const icon = (props.icon) ? <Icon name={props.icon}/> : null;

   return <a
      //disabled={props.disabled}
      className={props.className}
      //type={props.type}
      onClick={props.onClick}>{icon} {props.title}</a>;
};