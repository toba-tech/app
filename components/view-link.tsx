import * as React from "react";
import { flux } from "lib/state/hub";
import { Action } from "modules/actions";

interface Props {
   href:string;
   title:string;
}

export default (props:Props) => <a onClick={onClick} href={"/" + props.href}>{props.title}</a>;

function onClick(e:any) {
   e.preventDefault();
   flux.emit(Action.ChangeView, e.target.getAttribute("href"));
}