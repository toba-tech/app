/* tslint:disable:no-unused-expression */
import { RouteList } from "./router";
import "mocha";
import { render, tag } from "./test-util";
import Icon from "./icon";
import App from "./app";

const routes:RouteList = {
   home: Icon
};

describe("App Component", () => {
   it("renders route list", () => {
      render(App, { routes }).expect({
         tagName: tag.div,
         className: "main-view"
      });
   });
});