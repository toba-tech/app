import "./notifications.scss";
import * as React from "react";
import Icon from "./icon";
//import log from '../worker';

interface State {
   notifications:string[];
}

export default class extends React.Component<undefined, State> {
   constructor() {
      super();
      this.state = {
         notifications: []
      };
   }
   componentDidMount() {
      // hz('notifications').watch().subscribe(
      //    n => { this.setState({ notifications: n }); },
      //    err => { log.error(err); }
      // );
   }
   render() {
      return (
         <div className="notification-menu">
            <Icon name="bell"/>
            <ul>
               { this.state.notifications.map(n => <li>{n}</li>) }
            </ul>
         </div>
      );
   }
}