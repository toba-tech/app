/* tslint:disable:no-unused-expression */
import "mocha";
import { render, tag } from "./test-util";
import Footer from "./footer";

describe("Footer Component", () => {
   it("renders footer tag", () => {
      render(Footer).expect({ tagName: tag.footer });
   });
});