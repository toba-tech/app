/* tslint:disable:no-unused-expression */
import "mocha";
import { render, tag } from "./test-util";
import TextInput from "./text-input";

describe("Text Input Component", () => {
   it("shows a simple text input", () => {
      render(TextInput).expect({
         tagName: tag.div,
         className: "text-input"
      });
   });
});