package db

import (
	"toba.tech/app/lib/db/schema"
)

type existsQuery struct {
	query
}

func ExistsIn(table *schema.Table) *existsQuery {
	q := &existsQuery{}
	q.setTable(table)
	return q
}

func ExistsInTable(name string) *existsQuery {
	q := &existsQuery{}
	q.tableName = name
	return q
}

func (q *existsQuery) Where(conditions string, parameters ...interface{}) *existsQuery {
	q.where(conditions, parameters...)
	return q
}

// String builds a SELECT statement using the table name and SQL literal
// conditions.
func (q *existsQuery) String() string {
	return "SELECT EXISTS (SELECT * FROM " + q.tableName + " WHERE " + q.conditions + ");"
}

func (q *existsQuery) Run(ex Queryer) (bool, error) {
	var exists bool
	err := q.getOne(ex, q, &exists)
	return exists, err
}

func (cx *Connection) HasTable(tableName string) bool {
	return hasTable(cx, tableName)
}

func (tx *Transaction) HasTable(tableName string) bool {
	return hasTable(tx, tableName)
}

// HasTable indicates whether the currently connected database has the named
// table by looking for it in the system `sqlite_master` table.
func hasTable(ex Queryer, tableName string) bool {
	exists, _ := ExistsInTable("sqlite_master").
		Where("type=? AND name=?", "table", tableName).
		Run(ex)

	return exists
}
