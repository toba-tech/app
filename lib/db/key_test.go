package db_test

import (
	"bytes"
	"testing"

	"toba.tech/app/lib/db"

	"github.com/stretchr/testify/assert"
)

func TestKeyUniqueness(t *testing.T) {
	key1 := db.NewKey()
	key2 := db.NewKey()

	assert.NotEqual(t, key1, key2)
}

func TestKeyQuery(t *testing.T) {
	cx, key := addTestRow(t, 40)

	exists, err := db.ExistsIn(secondTable).Where("moduleID = ?", 40).Run(cx)
	assert.NoError(t, err)
	assert.True(t, exists)

	rows, err := db.Select("tenantKey").From(secondTable).Where("moduleID = ?", 40).Run(cx)
	assert.NoError(t, err)
	assert.NotNil(t, rows)
	var tenantKey []byte

	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(&tenantKey)
		assert.NoError(t, err)
		assert.True(t, bytes.Equal(key, tenantKey))
	}
	assert.NoError(t, rows.Err())

	exists, err = db.ExistsIn(secondTable).Where("tenantKey = " + key.HexString()).Run(cx)
	assert.NoError(t, err)
	assert.True(t, exists)
}

func TestKeyString(t *testing.T) {
	key := db.NewKey()
	short, err := db.KeyString(key)
	assert.NoError(t, err)

	long := key.String()

	assert.Len(t, long, 32)
	assert.Len(t, short, 26)
}
