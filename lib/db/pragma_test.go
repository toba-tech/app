package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestPragma(t *testing.T) {
	cx, err := db.ConnectToPath(dataFilePath)
	assert.NoError(t, err)

	defer cx.Close()

	assert.Equal(t, 0, cx.SchemaVersion)

	err = cx.UpdateSchemaVersion(5)
	assert.NoError(t, err)

	assert.Equal(t, 5, cx.SchemaVersion)

	cx.Transact(func(tx *db.Transaction) error {
		err = tx.UpdateSchemaVersion(5)
		assert.NoError(t, err)
		return err
	})

}
