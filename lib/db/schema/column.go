package schema

import "bytes"

type (
	column struct {
		name            string
		pk              bool
		fk              string
		unique          bool
		kind            typeID
		allowNull       bool
		check           string
		defaultValue    string
		caseInsensitive bool
		compositeKey    bool
		cascadeDelete   bool
		cascadeUpdate   bool
		deferConstraint bool
	}

	typeID int
)

const (
	CURRENT_DATE      = "CURRENT_DATE"      // string date
	CURRENT_DATETIME  = "CURRENT_DATETIME"  // string date and time
	CURRENT_TIME      = "CURRENT_TIME"      // string time
	CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP" // integer unix timestamp
)

const (
	BlobType typeID = iota
	BoolType
	DateType
	IntType
	TextType
)

func make(name string, kind typeID) *column {
	return &column{name: name, kind: kind}
}

// IntColumn creates a column definition for type INTEGER.
func IntColumn(name string) *column {
	return make(name, IntType)
}

// RowIdColumn creates an INTEGER column defined as the PRIMARY KEY which is
// automatically treated by SQLite as an alias for the built-in RowID.
//
// https://sqlite.org/autoinc.html
func RowIdColumn(name string) *column {
	return IntColumn(name).IsKey()
}

// TextColumn creates a column definition for type TEXT.
func TextColumn(name string) *column {
	return make(name, TextType)
}

// StatusColumn creates an INTEGER column that defaults to 0.
func StatusColumn(name string) *column {
	return IntColumn(name).Default("0")
}

func BooleanColumn(name string) *column {
	return make(name, BoolType)
}

// BlobColumn creates a column of type BLOB.
func BlobColumn(name string) *column {
	return make(name, BlobType)
}

// UlidColumn creates column type BLOB with a UNIQUE constraint and a CHECK
// constraint ensuring the value is a 16 byte array.
func UlidColumn(name string) *column {
	return BlobColumn(name).
		Unique().
		Check("typeof(" + name + ")='blob' AND length(" + name + ")=16")
}

// DateColumn creates column type DATETIME.
func DateColumn(name string) *column {
	return make(name, DateType)
}

// TimeStampColumn creates column type DATETIME defaulting to CURRENT_TIMESTAMP.
func TimeStampColumn(name string) *column {
	return DateColumn(name).Default(CURRENT_TIMESTAMP)
}

// Check adds a CHECK constraint to the column.
func (c *column) Check(constraint string) *column {
	c.check = constraint
	return c
}

// IsKey marks the column as a PRIMARY KEY.
//
//    "Unless the column is an INTEGER PRIMARY KEY or the table is a WITHOUT
//		ROWID table or the column is declared NOT NULL, SQLite allows NULL values
//		in a PRIMARY KEY column"
//		https://sqlite.org/lang_createtable.html
//
func (c *column) IsKey() *column {
	c.pk = true
	c.allowNull = false
	return c
}

// Ref defines a foreign key references to another table column and enables
// `CASCADE DELETE` and `CASCADE UPDATE`.
func (c *column) Ref(table *Table, col string) *column {
	c.fk = table.Name + "(" + col + ")"
	c.cascadeDelete = true
	c.cascadeUpdate = true
	c.deferConstraint = true
	return c
}

// NoCascadeDelete disables automatic deletion of foreign key rows.
func (c *column) NoCascadeDelete() *column {
	c.cascadeDelete = false
	return c
}

// NoCascadeUpdate disables automatic update of foreign key rows.
func (c *column) NoCascadeUpdate() *column {
	c.cascadeUpdate = false
	return c
}

// NoCascade disables both cascading updates and deletions of foreign keys.
func (c *column) NoCascade() *column {
	c.cascadeDelete = false
	c.cascadeUpdate = false
	return c
}

// AllowNull removes the NOT NULL flags that are otherwise set on all columns.
func (c *column) AllowNull() *column {
	c.allowNull = true
	return c
}

// Default sets the DEFAULT value for the column.
func (c *column) Default(d string) *column {
	c.defaultValue = d
	return c
}

// CaseInsensitive
func (c *column) CaseInsensitive() *column {
	c.caseInsensitive = true
	return c
}

func (c *column) Unique() *column {
	c.unique = true
	return c
}

// String builds the SQLite column definition.
func (c *column) String() string {
	sql := &bytes.Buffer{}
	sql.WriteString(c.name)
	sql.WriteString(" ")
	sql.WriteString(typeName(c.kind))

	if !c.allowNull && !(c.pk && c.kind == IntType && !c.compositeKey) {
		sql.WriteString(" NOT NULL")
	}

	if c.unique {
		sql.WriteString(" UNIQUE")
	}

	if c.pk && !c.compositeKey {
		sql.WriteString(" PRIMARY KEY")
	}

	if c.defaultValue != "" {
		sql.WriteString(" DEFAULT ")
		if c.kind == TextType {
			sql.WriteString("'")
			sql.WriteString(c.defaultValue)
			sql.WriteString("'")
		} else {
			sql.WriteString(c.defaultValue)
		}
	}

	if c.kind == TextType && c.caseInsensitive {
		sql.WriteString(" COLLATE NOCASE")
	}

	if c.check != "" {
		sql.WriteString(" CHECK(")
		sql.WriteString(c.check)
		sql.WriteString(")")
	}

	return sql.String()
}

func typeName(t typeID) string {
	switch t {
	case BlobType:
		return "BLOB"
	case BoolType:
		return "BOOLEAN"
	case DateType:
		return "DATETIME"
	case IntType:
		return "INTEGER"
	case TextType:
		return "TEXT"
	}
	return ""
}
