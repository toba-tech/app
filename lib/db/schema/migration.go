package schema

import "fmt"

type (
	Migration []fmt.Stringer

	AddColumn struct {
		column *column
		table  *Table
	}

	DropColumn struct {
		name  string
		table *Table
	}
)

// Migration is a simple list of SQL statements to be executed in a
// transaction.
func Migrate(add ...fmt.Stringer) Migration {
	return Migration(add)
}

func (a *AddColumn) String() string {
	return "ALTER TABLE " + a.table.Name + " ADD " + a.column.String() + ";"
}

func (d *DropColumn) String() string {
	return "ALTER TABLE " + d.table.Name + " DROP " + d.name + ";"
}

func (t *Table) Add(c *column) *AddColumn {
	return &AddColumn{table: t, column: c}
}
