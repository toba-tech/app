package schema_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"toba.tech/app/lib/db/schema"
)

const basic = "CREATE TABLE IF NOT EXISTS test_table "
const cols = "col1 INTEGER NOT NULL, col2 INTEGER NOT NULL"
const pk = "PRIMARY KEY(col1, col2)"

func TestBasicTable(t *testing.T) {
	table := schema.Create("test_table").Columns(
		schema.IntColumn("col1").IsKey(),
		schema.IntColumn("col2"),
	)

	assert.Equal(t, basic+"(col1 INTEGER PRIMARY KEY, col2 INTEGER NOT NULL);", table.String())
}

func TestCompositeKeyTable(t *testing.T) {
	table := schema.Create("test_table").Columns(
		schema.IntColumn("col1").IsKey(),
		schema.IntColumn("col2").IsKey(),
		schema.TextColumn("col3").AllowNull(),
	)

	assert.Equal(t, basic+"("+cols+", col3 TEXT, "+pk+");", table.String())
}

func TestForeignKeyTable(t *testing.T) {
	other := schema.Create("other").Columns(
		schema.RowIdColumn("id"),
	)

	table := schema.Create("test_table").WithoutRowID().Columns(
		schema.IntColumn("col1").IsKey(),
		schema.IntColumn("col2").IsKey().Ref(other, "id"),
		schema.TextColumn("col3").AllowNull(),
	)

	assert.Equal(t, basic+"("+cols+", col3 TEXT, "+pk+", FOREIGN KEY(col2) REFERENCES other(id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED) WITHOUT ROWID;", table.String())

	table = schema.Create("test_table").WithoutRowID().Columns(
		schema.IntColumn("col1").IsKey(),
		schema.IntColumn("col2").IsKey().Ref(other, "id").NoCascadeDelete(),
		schema.TextColumn("col3").AllowNull(),
	)

	assert.Equal(t, basic+"("+cols+", col3 TEXT, "+pk+", FOREIGN KEY(col2) REFERENCES other(id) ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED) WITHOUT ROWID;", table.String())
}
