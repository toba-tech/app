package schema

import (
	"bytes"
)

// SqlTable defines the basic structure of a table. Column definitions are
// stored as a string literal since there is no advantage to abstracting
// their names and properties.
type Table struct {
	Name string
	// Whether to disable use of SQLite's built-in ROWID as the clustered
	// index instead of user-defined primary key(s). The default is False.
	// From the documentation:
	//
	// 	If a rowid table has a primary key that consists of a single column
	//		and the declared type of that column is `INTEGER` in any mixture of
	//		upper and lower case, then the column becomes an alias for the
	//		rowid. Such a column is usually referred to as an "integer primary
	//		key". A PRIMARY KEY column only becomes an integer primary key if
	//		the declared type name is exactly `INTEGER`. Other integer type
	//		names like `INT` or `BIGINT` or `SHORT INTEGER` or "UNSIGNED
	//		INTEGER" causes the primary key column to behave as an ordinary
	//		table column with integer affinity and a unique index, not as an
	//		alias for the rowid.
	//
	//		https://www.sqlite.org/lang_createtable.html
	NoRowID bool
	// Map of index names to their definition.
	Indexes map[string]string
	// Optional method to call after table has been created.
	//OnCreate func(q db.Queryer) error
	columns []*column
}

// Create makes a struct containing a SQL table definition.
// https://sqlite.org/lang_createtable.html
func Create(tableName string) *Table {
	return &Table{Name: tableName}
}

// String generates the SQL statement to create the table.
func (t *Table) String() string {
	fk := &bytes.Buffer{}
	pk := &bytes.Buffer{}
	sql := &bytes.Buffer{}
	sql.WriteString("CREATE TABLE IF NOT EXISTS ")
	sql.WriteString(t.Name)
	sql.WriteString(" (")

	keys := []*column{}

	for _, c := range t.columns {
		if c.pk {
			keys = append(keys, c)
		}
		if c.fk != "" {
			fk.WriteString(", ")
			fk.WriteString("FOREIGN KEY(")
			fk.WriteString(c.name)
			fk.WriteString(") REFERENCES ")
			fk.WriteString(c.fk)

			if c.cascadeDelete {
				fk.WriteString(" ON DELETE CASCADE")
			}
			if c.cascadeUpdate {
				fk.WriteString(" ON UPDATE CASCADE")
			}
			if c.deferConstraint {
				fk.WriteString(" DEFERRABLE INITIALLY DEFERRED")
			}
		}
	}
	if len(keys) > 1 {
		pk.WriteString(", PRIMARY KEY(")
		for i, c := range keys {
			if i > 0 {
				pk.WriteString(", ")
			}
			pk.WriteString(c.name)
			c.compositeKey = true
		}
		pk.WriteString(")")
	}

	for i, c := range t.columns {
		if i > 0 {
			sql.WriteString(", ")
		}
		sql.WriteString(c.String())
	}

	sql.Write(pk.Bytes())
	sql.Write(fk.Bytes())
	sql.WriteString(")")

	if t.NoRowID {
		sql.WriteString(" WITHOUT ROWID")
	}
	sql.WriteString(";")

	return sql.String()
}

// Columns adds column definitions to the table.
func (t *Table) Columns(columns ...*column) *Table {
	t.columns = columns
	return t
}

func (t *Table) WithoutRowID() *Table {
	t.NoRowID = true
	return t
}
