package schema_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"toba.tech/app/lib/db/schema"
)

func TestMigration(t *testing.T) {
	table := schema.Create("test_table").Columns(
		schema.IntColumn("col1").IsKey(),
		schema.IntColumn("col2"),
	)

	m := table.Add(schema.IntColumn("col3"))
	assert.Equal(t, "ALTER TABLE test_table ADD col3 INTEGER NOT NULL;", m.String())

	m = table.Add(schema.TextColumn("col4").CaseInsensitive().Default("foo"))
	assert.Equal(t, "ALTER TABLE test_table ADD col4 TEXT NOT NULL DEFAULT 'foo' COLLATE NOCASE;", m.String())
}
