package schema_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db/schema"
)

func TestBasicColumnTypes(t *testing.T) {
	c := schema.IntColumn("col")
	assert.Equal(t, "col INTEGER NOT NULL", c.String())

	c = schema.TextColumn("col")
	assert.Equal(t, "col TEXT NOT NULL", c.String())

	c = schema.DateColumn("col")
	assert.Equal(t, "col DATETIME NOT NULL", c.String())

	c = schema.BlobColumn("col")
	assert.Equal(t, "col BLOB NOT NULL", c.String())
}

func TestColumnModifiers(t *testing.T) {
	c := schema.IntColumn("col").AllowNull()
	assert.Equal(t, "col INTEGER", c.String())

	c = schema.IntColumn("col").IsKey()
	assert.Equal(t, "col INTEGER PRIMARY KEY", c.String())

	c = schema.TextColumn("col").IsKey()
	assert.Equal(t, "col TEXT NOT NULL PRIMARY KEY", c.String())

	c = schema.BooleanColumn("col")
	assert.Equal(t, "col BOOLEAN NOT NULL", c.String())

	c = schema.IntColumn("col").IsKey().AllowNull()
	assert.Equal(t, "col INTEGER PRIMARY KEY", c.String())

	c = schema.IntColumn("col").Unique()
	assert.Equal(t, "col INTEGER NOT NULL UNIQUE", c.String())

	c = schema.TextColumn("col").Default("foo")
	assert.Equal(t, "col TEXT NOT NULL DEFAULT 'foo'", c.String())

	c = schema.TextColumn("col").CaseInsensitive()
	assert.Equal(t, "col TEXT NOT NULL COLLATE NOCASE", c.String())
}

func TestSpecialColumnTypes(t *testing.T) {
	c := schema.RowIdColumn("col")
	assert.Equal(t, "col INTEGER PRIMARY KEY", c.String())

	c = schema.TimeStampColumn("col")
	assert.Equal(t, "col DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP", c.String())

	c = schema.StatusColumn("col")
	assert.Equal(t, "col INTEGER NOT NULL DEFAULT 0", c.String())

	c = schema.UlidColumn("col")
	assert.Equal(t, "col BLOB NOT NULL UNIQUE CHECK(typeof(col)='blob' AND length(col)=16)", c.String())
}
