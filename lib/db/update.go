package db

import (
	"bytes"

	"toba.tech/app/lib/db/schema"
)

type updateQuery struct {
	query
}

func Update(table *schema.Table) *updateQuery {
	q := &updateQuery{}
	q.setTable(table)
	return q
}

func (q *updateQuery) Set(column string, value interface{}) *updateQuery {
	q.parameters = append(q.parameters, value)
	q.columns = append(q.columns, column)
	return q
}

func (q *updateQuery) Where(conditions string, parameters ...interface{}) *updateQuery {
	q.where(conditions, parameters...)
	return q
}

// String generates an insertion query for a list of columns with
// parameter placholders for the values.
func (q *updateQuery) String() string {
	sql := &bytes.Buffer{}
	sql.WriteString("UPDATE ")
	sql.WriteString(q.tableName)
	sql.WriteString(" SET ")

	for i, c := range q.columns {
		if i > 0 {
			sql.WriteString(", ")
		}
		sql.WriteString(c)
		sql.WriteString("=?")
	}

	if q.conditions != "" {
		sql.WriteString(" WHERE ")
		sql.WriteString(q.conditions)
	}

	sql.WriteString(";")

	return sql.String()
}

// Run executes the update query and returns the number of rows affected.
func (q *updateQuery) Run(ex Queryer) (int64, error) {
	res, err := q.run(ex, q)
	if err != nil {
		return 0, err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil
}
