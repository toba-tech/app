package db

import "toba.tech/app/lib/db/schema"

// Create executes the query to create a table.
func Create(ex Queryer, table *schema.Table) error {
	_, err := ex.Exec(table.String())
	return err
}

// EnsureTables creates all defined tables in a transaction if they don't
// already exist in the attached database.
func (cx *Connection) EnsureTables(tables ...*schema.Table) error {
	return cx.Transact(func(tx *Transaction) error {
		for _, t := range tables {
			err := Create(tx, t)
			if err != nil {
				return err
			}
			// if t.OnCreate != nil {
			// 	err = t.OnCreate(tx)
			// }
		}
		return nil
	})
}
