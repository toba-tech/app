// Package db has basic methods for connecting to and running queries against
// SQLite databases. http://go-database-sql.org/
package db

import (
	"database/sql"
	"errors"
	"log"
	"os"
	"path/filepath"

	sqlite "github.com/mattn/go-sqlite3"

	"toba.tech/app/lib/config"
)

// engine is the custom name for the SQLite connection that includes
// function(s) registered by Initialize().
const engine = "toba_sqlite"

var (
	Path                string
	connections         = &ConnectionMap{Connections: make(map[string]*Connection)}
	ErrNoMatchingRecord = errors.New("No matching record")
	ErrNoFileName       = errors.New("No database file name given")
)

// Connect returns database connection to module database. Data files are
// sharded per module.
func Connect(fileName string) (*Connection, error) {
	return ConnectToPath(filepath.Join(Path, fileName+".db"))
}

// ConnectToPath connects to a SQLite database file. The Go SQL library doesn't
// create a driver level connection until a statement is executed.
//
// http://stackoverflow.com/questions/35804884/sqlite-concurrent-writing-performance
func ConnectToPath(filePath string) (*Connection, error) {
	if cx, ok := connections.get(filePath); ok {
		return cx, nil
	}

	sqlDB, err := sql.Open(engine, filePath)
	if err != nil {
		return nil, err
	}
	sqlDB.SetMaxOpenConns(1)
	cx := &Connection{DB: sqlDB}
	ver, err := GetSchemaVersion(cx)
	if err != nil {
		return nil, err
	}
	cx.SchemaVersion = ver
	cx.fileName = filePath

	connections.add(cx)

	return cx, nil
}

// Initialize applies configuration and registers a custom function to execute
// PRAGMA settings on each connection.
//
// https://www.thepolyglotdeveloper.com/2017/04/using-sqlite-database-golang-application/
func Initialize(config config.Database) error {
	Path = config.Path

	log.Printf("Initializing data path %s", Path)

	err := os.MkdirAll(Path, os.ModePerm)
	if err != nil {
		return err
	}

	sql.Register(engine, &sqlite.SQLiteDriver{
		ConnectHook: func(conn *sqlite.SQLiteConn) error {
			_, err := conn.Exec(pragmaQuery(DatabasePragma, ConnectionPragma), nil)
			return err
		},
	})

	return nil
}
