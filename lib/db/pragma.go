package db

import (
	"bytes"
	"strconv"

	"toba.tech/app/lib/db/pragma"
)

// pragmaQuery builds and combines PRAGMA assignment statements.
func pragmaQuery(maps ...map[string]string) string {
	sql := &bytes.Buffer{}

	for _, m := range maps {
		for k, v := range m {
			sql.WriteString("PRAGMA ")
			sql.WriteString(k)
			sql.WriteString(" = ")
			sql.WriteString(v)
			sql.WriteString(";")
		}
	}
	return sql.String()
}

// SetPragma sets a SQLite pragma value.
func SetPragma(ex Queryer, name, value string) error {
	_, err := ex.Exec("PRAGMA " + name + " = " + value + ";")
	return err
}

// GetPragma assigns a SQLite pragma value to a pointer.
func GetPragma(ex Queryer, name string, value interface{}) error {
	return ex.QueryRow("PRAGMA " + name + ";").Scan(value)
}

// GetSchemaVersion gets the SQLite schema pragma.
func GetSchemaVersion(ex Queryer) (int, error) {
	var version int
	err := GetPragma(ex, pragma.SchemaVersion, &version)
	if err != nil {
		return 0, err
	}
	return version, nil
}

func (cx *Connection) UpdateSchemaVersion(version int) error {
	err := updateSchemaVersion(cx, version)
	if err != nil {
		return err
	}
	cx.SchemaVersion = version
	return nil
}

func (tx *Transaction) UpdateSchemaVersion(version int) error {
	err := updateSchemaVersion(tx, version)
	if err != nil {
		return err
	}
	tx.cx.SchemaVersion = version
	return nil
}

func updateSchemaVersion(ex Queryer, version int) error {
	return SetPragma(ex, pragma.SchemaVersion, strconv.Itoa(version))
}
