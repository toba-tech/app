package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestSchemaCreation(t *testing.T) {
	cx, err := db.ConnectToPath(dataFilePath)
	assert.NoError(t, err)

	assert.True(t, cx.HasTable("first"))
	assert.True(t, cx.HasTable("second"))
	assert.False(t, cx.HasTable("nothing"))
}
