package db

import (
	"bytes"
	"strings"

	"toba.tech/app/lib/db/schema"
)

// InsertQuery generates an insertion query for a list of columns with
// parameter placholders for the values. If allowReplace is true then the
// values are allowed to replace an existing row with same primary key.
// Otherwise a unique constraint violation may occur.
type InsertQuery struct {
	query
	allowReplace bool
}

func InsertInto(table *schema.Table, columns ...string) *InsertQuery {
	q := &InsertQuery{}
	q.setTable(table)
	q.columns = columns
	return q
}

func UpsertInto(table *schema.Table, fields ...string) *InsertQuery {
	q := InsertInto(table, fields...)
	q.allowReplace = true
	return q
}

func (q *InsertQuery) Values(values ...interface{}) *InsertQuery {
	q.parameters = values
	return q
}

func (q *InsertQuery) String() string {
	sql := &bytes.Buffer{}
	sql.WriteString("INSERT")

	if q.allowReplace {
		sql.WriteString(" OR REPLACE")
	}

	sql.WriteString(" INTO ")
	sql.WriteString(q.tableName)
	sql.WriteString(" (")
	sql.WriteString(strings.Join(q.columns, ", "))
	sql.WriteString(") VALUES (")
	sql.WriteString(strings.TrimRight(strings.Repeat("?, ", len(q.columns)), ", "))
	sql.WriteString(");")

	return sql.String()
}

func (q *InsertQuery) Run(ex Queryer) (int64, error) {
	res, err := q.run(ex, q)
	if err != nil {
		return 0, err
	}

	if !q.noRowID {
		id, err := res.LastInsertId()
		if err != nil {
			return 0, err
		}
		return id, nil
	}
	return 0, err
}
