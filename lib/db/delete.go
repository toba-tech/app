package db

import (
	"bytes"

	"toba.tech/app/lib/db/schema"
)

type DeleteQuery struct {
	query
}

func DeleteFrom(table *schema.Table) *DeleteQuery {
	q := &DeleteQuery{}
	q.setTable(table)
	return q
}

func (q *DeleteQuery) Where(conditions string, parameters ...interface{}) *DeleteQuery {
	q.where(conditions, parameters...)
	return q
}

// String builds a DELETE statement using the table name and SQL literal
// conditions.
func (q *DeleteQuery) String() string {
	sql := &bytes.Buffer{}
	sql.WriteString("DELETE FROM ")
	sql.WriteString(q.tableName)

	if q.conditions != "" {
		sql.WriteString(" WHERE ")
		sql.WriteString(q.conditions)
	}
	sql.WriteString(";")
	return sql.String()
}

func (q *DeleteQuery) Run(ex Queryer) (int64, error) {
	res, err := q.run(ex, q)
	if err != nil {
		return 0, err
	}
	affected, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}
	return affected, nil

}
