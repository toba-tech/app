package db

import (
	"database/sql"

	"toba.tech/app/lib/db/pragma"

	_ "github.com/mattn/go-sqlite3"
)

type (
	// Database connection.
	Connection struct {
		*sql.DB
		fileName      string
		SchemaVersion int // http://www.sqlite.org/pragma.html#pragma_user_version
	}

	Transaction struct {
		*sql.Tx
		cx *Connection
	}

	// Queryer is an implicit (regular connection) or explicit SQLite
	// transaction. It is matched by sql.DB and sql.Tx.
	Queryer interface {
		Prepare(string) (*sql.Stmt, error)
		Exec(string, ...interface{}) (sql.Result, error)
		QueryRow(string, ...interface{}) *sql.Row
		Query(string, ...interface{}) (*sql.Rows, error)
	}

	WithTx func(*Transaction) error
)

// DatabasePragma defines settings that can be applied only when the database
// is first created.
var DatabasePragma = map[string]string{
	pragma.JournalMode: "WAL", // Write-ahead log
}

// ConnectionPragma defines settings that must be re-specified for every
// connection.
//
// https://codificar.com.br/blog/sqlite-optimization-faq/
var ConnectionPragma = map[string]string{
	pragma.AutoIndex:   "ON",
	pragma.ForeignKeys: "ON",
	pragma.Synchronous: "NORMAL",
	pragma.TempStore:   "MEMORY",
}

// Close the underlying SQL connection after removing it from the cache.
func (cx *Connection) Close() error {
	connections.remove(cx)
	return cx.DB.Close()
}

// Begin transaction.
func (cx *Connection) Begin() (*Transaction, error) {
	tx, err := cx.DB.Begin()
	if err != nil {
		return nil, err
	}
	return &Transaction{Tx: tx, cx: cx}, nil
}

// Transaction creates a new transaction and executes everything in a callback
// function within that transaction. If the callback returns an error then the
// transaction is rolled back.
//
// https://astaxie.gitbooks.io/build-web-application-with-golang/en/05.3.html
func (cx *Connection) Transact(fn WithTx) error {
	tx, err := cx.Begin()
	if err != nil {
		return err
	}
	err = fn(tx)

	if err != nil {
		tx.Rollback()
	} else {
		tx.Commit()
	}
	return err
}
