package db_test

import (
	"testing"

	"toba.tech/app/lib/db"
)

var testCache = db.ConnectionMap{Connections: make(map[string]*db.Connection)}

// https://hackernoon.com/non-blocking-code-in-go-part-1-6c13971c47b6
func BenchmarkCacheSet(b *testing.B) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		cx := &db.Connection{}
		testCache.Connections["something"] = cx
	}
	//_ = o
}

// func BenchmarkCacheGet(b *testing.B) {
// 	b.ReportAllocs()

// 	var o bool

// 	for i := 0; i < b.N; i++ {
// 		o = sb.Value()
// 	}

// 	//_ = o
// }
