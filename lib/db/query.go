package db

import (
	"database/sql"

	"toba.tech/app/lib/db/schema"
)

type (
	// query contains basic elements defining a SQL query. The approach is
	// similar to https://github.com/gocraft/dbr
	query struct {
		tableName string
		noRowID   bool
		columns   []string
		//TenantID   int64
		conditions string
		parameters []interface{}
	}

	// queryBuilder represents the most basic elements of a query: the SQL
	// command and its parameters.
	queryBuilder interface {
		String() string
		Parameters() []interface{}
	}

	// ExecQuery describes queries that return no rows.
	ExecQuery interface {
		Run(ex Queryer) (int64, error)
	}

	// RowsQuery describes queries that may return rows. They may return basic
	// row structs, the first row marshalled into a given interface, or all rows
	// marshalled into an array of interface.
	RowsQuery interface {
		Run(ex Queryer) (*sql.Rows, error)
		First(ex Queryer, vars ...interface{}) error
		Into(ex Queryer, fn RowReceiver, vars ...interface{}) ([]interface{}, error)
	}
)

// setTable records the name of the table to be queried.
func (q *query) setTable(table *schema.Table) {
	q.tableName = table.Name
	q.noRowID = table.NoRowID
}

// Parameters returns the list of parameters as required by the queryBuilder
// interface.
func (q *query) Parameters() []interface{} {
	return q.parameters
}

// where adds the condition with `?` placeholders and parameters.
func (q *query) where(conditions string, parameters ...interface{}) {
	q.conditions = conditions
	q.parameters = parameters
}

// run executes a query without returning rows.
func (q *query) run(ex Queryer, b queryBuilder) (sql.Result, error) {
	stmt, err := q.prepare(ex, b)
	if err != nil {
		return nil, err
	}
	return stmt.Exec(b.Parameters()...)
}

// get returns all rows matching the query.
func (q *query) get(ex Queryer, b queryBuilder) (*sql.Rows, error) {
	stmt, err := q.prepare(ex, b)
	if err != nil {
		return nil, err
	}
	return stmt.Query(b.Parameters()...)
}

// getOne populates the vars with the first row matching the query.
func (q *query) getOne(ex Queryer, b queryBuilder, vars ...interface{}) error {
	stmt, err := q.prepare(ex, b)
	if err != nil {
		return err
	}
	return stmt.QueryRow(q.Parameters()...).Scan(vars...)
}

// prepare executes the database driver's Prepare method on the query string.
func (q *query) prepare(ex Queryer, b queryBuilder) (*sql.Stmt, error) {
	return ex.Prepare(b.String())
}
