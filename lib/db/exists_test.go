package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestExistsQuery(t *testing.T) {
	expect := "SELECT EXISTS (SELECT * FROM test WHERE something > 0 AND else = 'foo');"
	query := db.ExistsIn(testTable).Where("something > 0 AND else = 'foo'").String()
	assert.Equal(t, expect, query)
}
