package db

import (
	"bytes"
	"database/sql"
	"strconv"
	"strings"

	"toba.tech/app/lib/db/schema"
)

type selectQuery struct {
	query
	limit    uint
	distinct bool
	orderBy  string
}

// RowReceiver is a function that converts a `sql.Row` into typed results.
type RowReceiver func() interface{}

func Select(columns ...string) *selectQuery {
	q := &selectQuery{}
	q.columns = columns
	return q
}

func (q *selectQuery) From(table *schema.Table) *selectQuery {
	q.setTable(table)
	return q
}

func (q *selectQuery) Where(conditions string, parameters ...interface{}) *selectQuery {
	q.where(conditions, parameters...)
	return q
}

func (q *selectQuery) Limit(count uint) *selectQuery {
	q.limit = count
	return q
}

func (q *selectQuery) Distinct() *selectQuery {
	q.distinct = true
	return q
}

func (q *selectQuery) OrderBy(field string) *selectQuery {
	q.orderBy = field
	return q
}

// String builds a SELECT statement using the table name and SQL literal
// conditions.
func (q *selectQuery) String() string {
	sql := &bytes.Buffer{}
	sql.WriteString("SELECT ")

	if q.distinct {
		sql.WriteString("DISTINCT ")
	}

	if len(q.columns) > 0 {
		sql.WriteString(strings.Join(q.columns, ", "))
	} else {
		sql.WriteString("*")
	}

	sql.WriteString(" FROM ")
	sql.WriteString(q.tableName)

	if q.conditions != "" {
		sql.WriteString(" WHERE ")
		sql.WriteString(q.conditions)
	}

	if q.orderBy != "" {
		sql.WriteString(" ORDER BY ")
		sql.WriteString(q.orderBy)
	}

	if q.limit > 0 {
		sql.WriteString(" LIMIT ")
		sql.WriteString(strconv.FormatUint(uint64(q.limit), 10))
	}

	sql.WriteString(";")

	return sql.String()
}

func (q *selectQuery) Run(ex Queryer) (*sql.Rows, error) {
	return q.get(ex, q)
}

func (q *selectQuery) First(ex Queryer, vars ...interface{}) error {
	q.limit = 1
	return q.getOne(ex, q, vars...)
}

func (q *selectQuery) Into(ex Queryer, fn RowReceiver, vars ...interface{}) ([]interface{}, error) {
	rows, err := q.Run(ex)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	list := []interface{}{}

	for rows.Next() {
		if err := rows.Scan(vars...); err != nil {
			return nil, err
		}
		list = append(list, fn())
	}
	return list, rows.Err()
}
