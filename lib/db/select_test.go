package db_test

import (
	"bytes"
	"database/sql"
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestBasicSelectQuery(t *testing.T) {
	expect := "SELECT field1, field2 FROM test WHERE field1 > 2;"
	query := db.
		Select("field1", "field2").
		From(testTable).
		Where("field1 > 2")

	assert.Equal(t, expect, query.String())

	expect = "SELECT * FROM test WHERE field1 = 2;"
	query = db.Select().From(testTable).Where("field1 = 2")

	assert.Equal(t, expect, query.String())
}

func TestDistinctQuery(t *testing.T) {
	expect := "SELECT DISTINCT field1, field2 FROM test WHERE field1 > ?;"
	query := db.
		Select("field1", "field2").
		Distinct().
		From(testTable).
		Where("field1 > ?", 3)

	assert.Equal(t, expect, query.String())
}

func TestLimitQuery(t *testing.T) {
	expect := "SELECT field1, field2 FROM test WHERE field2 > ? LIMIT 10;"
	query := db.
		Select("field1", "field2").
		From(testTable).
		Where("field2 > ?", 3).
		Limit(10)

	assert.Equal(t, expect, query.String())
}

func TestOrderByQuery(t *testing.T) {
	expect := "SELECT field1, field2 FROM test WHERE field1 > ? ORDER BY field1;"
	query := db.
		Select("field1", "field2").
		From(testTable).
		Where("field1 > ?", 3).
		OrderBy("field1")

	assert.Equal(t, expect, query.String())
}

func TestSelectWithoutCondition(t *testing.T) {
	expect := "SELECT field1, field2 FROM test;"
	query := db.
		Select("field1", "field2").
		From(testTable)

	assert.Equal(t, expect, query.String())
}

func TestSelectInto(t *testing.T) {
	cx, err := db.ConnectToPath(dataFilePath)
	assert.NoError(t, err)

	_, err = db.DeleteFrom(secondTable).Run(cx)
	assert.NoError(t, err)

	_, err = db.DeleteFrom(firstTable).Run(cx)
	assert.NoError(t, err)

	testId1 := 27
	testId2 := 28
	_, testKey1 := addTestRow(t, testId1)
	_, testKey2 := addTestRow(t, testId2)

	type row struct {
		id  int
		key []byte
	}
	var id int
	var key []byte

	// https://github.com/golang/go/wiki/InterfaceSlice
	list, err := db.
		Select("moduleID", "tenantKey").
		From(secondTable).
		OrderBy("moduleID").
		Into(cx, func() interface{} { return row{id: id, key: key} },
			&id, &key)

	assert.NoError(t, err)
	assert.Len(t, list, 2)
	assert.True(t, bytes.Equal(testKey1, list[0].(row).key))
	assert.Equal(t, testId1, list[0].(row).id)
	assert.True(t, bytes.Equal(testKey2, list[1].(row).key))
	assert.Equal(t, testId2, list[1].(row).id)
}

func TestSelectFirst(t *testing.T) {
	cx, err := db.ConnectToPath(dataFilePath)
	assert.NoError(t, err)

	testID := 26

	addTestRow(t, testID)

	var id int
	err = db.Select("moduleID").From(secondTable).Where("moduleID = ?", testID).First(cx, &id)
	assert.NoError(t, err)
	assert.Equal(t, testID, id)

	err = db.Select("moduleID").From(secondTable).Where("moduleID = ?", 99).First(cx, &id)
	assert.Error(t, err)
	assert.Equal(t, sql.ErrNoRows, err)
}
