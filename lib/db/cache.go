package db

import "sync"

// connectionMap is a thread-safe connection map.
// https://hackernoon.com/non-blocking-code-in-go-part-1-6c13971c47b6
type ConnectionMap struct {
	sync.RWMutex
	Connections map[string]*Connection
}

// add a connection to the cache map.
func (c *ConnectionMap) add(cx *Connection) *Connection {
	c.Lock()
	defer c.Unlock()
	c.Connections[cx.fileName] = cx
	return cx
}

// get a connection from the cache map.
func (c *ConnectionMap) get(fileName string) (*Connection, bool) {
	c.RLock()
	defer c.RUnlock()
	cx, ok := c.Connections[fileName]
	return cx, ok
}

// remove a connection from the cache map.
func (c *ConnectionMap) remove(cx *Connection) {
	c.Lock()
	defer c.Unlock()
	delete(c.Connections, cx.fileName)
}
