package db_test

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/config"
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
)

var (
	firstTable = schema.Create("first").WithoutRowID().Columns(
		schema.BlobColumn("id").IsKey(),
	)

	secondTable = schema.Create("second").WithoutRowID().Columns(
		schema.UlidColumn("tenantKey").IsKey().Ref(firstTable, "id"),
		schema.IntColumn("moduleID").IsKey(),
		schema.DateColumn("beginOn"),
		schema.DateColumn("endOn"),
	)

	dataFilePath string
)

func addTestRow(t *testing.T, moduleID int) (*db.Connection, db.Key) {
	cx, err := db.ConnectToPath(dataFilePath)
	assert.NoError(t, err)

	key := db.NewKey()
	err = cx.Transact(func(tx *db.Transaction) error {
		_, err = db.InsertInto(firstTable, "id").Values(key).Run(tx)
		if err != nil {
			return err
		}

		_, err = db.
			InsertInto(secondTable, "tenantKey", "moduleID", "beginOn", "endOn").
			Values(key, moduleID, schema.CURRENT_TIMESTAMP, schema.CURRENT_TIMESTAMP).
			Run(tx)

		return err
	})
	assert.NoError(t, err)

	return cx, key
}

// TestMain creates the data file and tables for all other `db_test` tests.
// Regular tests are only run if setup completes and sets status code to 0.
func TestMain(m *testing.M) {
	code := 1
	defer func() {
		if code == 0 {
			code = m.Run()
		}
		os.Exit(code)
	}()

	dir, err := ioutil.TempDir(os.TempDir(), "toba")
	if err != nil {
		log.Printf("Failed to create temporary directory\n%v\n", err)
		return
	}

	defer os.Remove(dir)

	_, err = os.Stat(dir)
	if err != nil {
		log.Printf("Failed to get %s details\n%v\n", dir, err)
		return
	}

	dataFilePath = filepath.Join(dir, "toba")

	err = db.Initialize(config.Database{Path: dir})
	if err != nil {
		log.Printf("Failed to initialize database at %s\n%v\n", dir, err)
		return
	}

	cx, err := db.ConnectToPath(dataFilePath)
	if err != nil {
		log.Printf("Failed to connect to %s\n%v\n", dataFilePath, err)
		return
	}

	err = cx.EnsureTables(firstTable, secondTable)
	if err != nil {
		log.Printf("Failed to create tables in %s\n%v\n", dataFilePath, err)
		return
	}

	code = 0
}
