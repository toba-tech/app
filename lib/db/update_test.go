package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestUpdateQuery(t *testing.T) {
	expect := "UPDATE test SET one=?, two=?, three=?;"
	query := db.Update(testTable).Set("one", nil).Set("two", nil).Set("three", nil)
	assert.Equal(t, expect, query.String())
}

func TestUpdateQueryConditions(t *testing.T) {
	expect := "UPDATE test SET one=?, two=? WHERE one = ?;"
	query := db.
		Update(testTable).
		Set("one", nil).
		Set("two", nil).
		Where("one = ?", 2)

	assert.Equal(t, expect, query.String())
}
