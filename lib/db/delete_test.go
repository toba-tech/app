package db_test

import (
	"testing"

	"toba.tech/app/lib/db"

	"github.com/stretchr/testify/assert"
)

func TestDeleteQuery(t *testing.T) {
	expect := "DELETE FROM test WHERE one = ?;"
	query := db.DeleteFrom(testTable).Where("one = ?", 2)
	assert.Equal(t, expect, query.String())

	expect = "DELETE FROM test;"
	query = db.DeleteFrom(testTable)
	assert.Equal(t, expect, query.String())
}

func TestDelete(t *testing.T) {
	cx, _ := addTestRow(t, 52)

	exists, err := db.ExistsIn(secondTable).Where("moduleID = ?", 52).Run(cx)
	assert.NoError(t, err)
	assert.True(t, exists)

	affected, err := db.DeleteFrom(secondTable).Where("moduleID = ? ", 52).Run(cx)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), affected)

	exists, err = db.ExistsIn(secondTable).Where("moduleID = ?", 52).Run(cx)
	assert.NoError(t, err)
	assert.False(t, exists)
}
