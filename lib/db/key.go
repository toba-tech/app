package db

import (
	"encoding/hex"
	"math/rand"
	"strings"
	"sync"
	"time"

	"github.com/oklog/ulid"
)

type Key []byte

var randPool = sync.Pool{
	New: func() interface{} {
		t := time.Unix(1000000, 0)
		return rand.New(rand.NewSource(t.UnixNano()))
	},
}

// HexString returns the ULID in the hexadecimal format that can be used to
// query SQLite.
func (key Key) HexString() string {
	return "X'" + key.String() + "'"
}

// String converts a ULID key to the string format expected by SQLIte. This is
// different from the shorter string format supported internally by ULID.
func (key Key) String() string {
	return strings.ToUpper(hex.EncodeToString(key))
}

// KeyString converts a key to its short ULID represenatation.
func KeyString(key []byte) (string, error) {
	var id ulid.ULID
	err := id.UnmarshalBinary(key)
	if err != nil {
		return "", err
	}
	return id.String(), nil
}

// NewKey creates a ULID.
// https://github.com/oklog/ulid
func NewKey() Key {
	t := time.Unix(1000000, 0)
	entropy := randPool.Get().(*rand.Rand)
	defer randPool.Put(entropy)
	id, err := ulid.New(ulid.Timestamp(t), entropy)

	if err != nil {
		return nil
	}

	key, err := id.MarshalBinary()
	if err != nil {
		return nil
	}

	return key
}
