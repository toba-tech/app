package pragma

const (
	AutoIndex            = "automatic_index"    // https://sqlite.org/optoverview.html#autoindex
	DeferForeignKeyCheck = "defer_foreign_keys" // https://sqlite.org/pragma.html#pragma_defer_foreign_keys
	SchemaVersion        = "user_version"       // https://sqlite.org/pragma.html#pragma_user_version
	JournalMode          = "journal_mode"       // WAL to use write-head log
	ForeignKeys          = "foreign_keys"       // ON to enforce foreign keys
	Synchronous          = "synchronous"        // OFF to not wait for OS to report disk write success
	TempStore            = "temp_store"
)
