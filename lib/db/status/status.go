package status

type DataStatus int

const (
	Okay DataStatus = iota
	Pending
)
