package db_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/db"
)

func TestInsertQuery(t *testing.T) {
	expect := "INSERT INTO test (one, two, three) VALUES (?, ?, ?);"
	query := db.InsertInto(testTable, "one", "two", "three")
	assert.Equal(t, expect, query.String())

}

func TestInsertOrReplaceQuery(t *testing.T) {
	expect := "INSERT OR REPLACE INTO test (one, two, three) VALUES (?, ?, ?);"
	query := db.UpsertInto(testTable, "one", "two", "three").String()
	assert.Equal(t, expect, query)
}
