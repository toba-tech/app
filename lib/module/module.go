// Package module defines modules.
package module

// ModuleInfo describes a module which is a collection of service endpoints
// and SQL information to create and update storage for those services.
//
// The same source code path may also contain client components to be loaded
// at the module URL path.
//
// A module will usually provide multiple services and may use more than one
// data store, including data from other modules.
type ModuleInfo struct {
	// Path is the URL path to the client components, if any, and is also the
	// default Storage file name.
	Path string
	// ID is used to validate licensing.
	ID int
	// NoLicenseRequired should be true if the module can be used without a
	// license.
	NoLicenseRequired bool
	// NoClient should be true if the module has no client components to avoid
	// generating, caching and serving a template for the path.
	NoClient bool
	Services ServiceMap
	Stores   []*Storage
}

// Register uses the builder pattern to create information about a module path,
// services and data storage.
func Register(id int, path string) *ModuleInfo {
	return &ModuleInfo{
		Path:     path,
		ID:       id,
		Services: make(ServiceMap),
	}
}

// RequireLicense indicates if the module can be used by a tenant without an
// active license.
func (m *ModuleInfo) RequireLicense(require bool) *ModuleInfo {
	m.NoLicenseRequired = !require
	return m
}

// HasWebClient indicates whether the module has React client components. If
// not then no HTML template will be generated and cached for the module path.
func (m *ModuleInfo) HasWebClient(has bool) *ModuleInfo {
	m.NoClient = !has
	return m
}

// Storage records the database files used for module storage.
func (m *ModuleInfo) Storage(stores ...*Storage) *ModuleInfo {
	m.Stores = stores
	return m
}

// Add a service endpoint to the module.
func (m *ModuleInfo) Add(id ServiceID, ep *Endpoint) *ModuleInfo {
	m.Services[id] = ep
	return m
}

// Migrate runs migrations on all module stores. The module path is added to
// the storage struct to be used as part of the data file name if a name hasn't
// been defined.
//
// Method aborts if module has no data stores otherwise migration is initiated
// for each store.
func (m *ModuleInfo) Migrate() error {
	if m.Stores == nil {
		return nil
	}

	for _, s := range m.Stores {
		err := s.migrate()
		if err != nil {
			return err
		}
	}
	return nil
}

// Amalgamate combines modules service maps and paths so that service references
// can be retrieved directly from the combined map without without iterating
// modules for every service call.
func Amalgamate(modules []*ModuleInfo) (ServiceMap, []string) {
	services := make(ServiceMap)
	paths := []string{}

	for _, m := range modules {
		for k, v := range m.Services {
			services[k] = v
		}
		if !m.NoClient {
			paths = append(paths, m.Path)
		}
	}

	return services, paths
}
