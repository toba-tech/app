/**
 * Module defined actions transmitted through websocket and executed by Go.
 */
export namespace Service {
   /**
    * Authentication and authorization requirements
    */
   export interface Auth {
      /** Whether action can be invoked by unauthenticated user */
      allowAnonymous:boolean;
      /** Permissions required to invoke the action */
      permissions:number[];
   }

   export interface Request<T> {
      /** Unique identifier round-tripped with message. */
      id:string;
      type:number;
      data:T;
      /** Javascript WebToken. */
      jwt:string;
      /** Whether to receive notice of query result changes. */
      subscribe:boolean;
   }

   /**
    * Match server `Response` definition in `lib/module/service.go`.
    */
   export interface Response {
      /** Unique identifier round-tripped with message. */
      id:string;
      status:number;
      data:any;
   }
}