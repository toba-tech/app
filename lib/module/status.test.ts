/* tslint:disable:no-unused-expression */
import "mocha";
import { expect } from "chai";
import { Status } from "./status";

describe("Service Statuses", () => {
   it("are generated", () => {
      expect(Status.Okay).to.exist;
   });
});