package module

import (
	"log"

	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
)

// Storage defines data storage for a module, including its file name, the
// tables it contains and migrations to new schemas.
type Storage struct {
	FileName   string
	Schemas    []*schema.Table
	Migrations []schema.Migration
}

// Database creates new Storage struct.
func Database(name string, tables ...*schema.Table) *Storage {
	return &Storage{FileName: name, Schemas: tables}
}

// Connect to database for module storage.
func (s *Storage) Connect() (*db.Connection, error) {
	if s.FileName == "" {
		return nil, db.ErrNoFileName
	}
	return db.Connect(s.FileName)
}

// initialize creates module tables for a particular tenant. The initial schema
// version will be greater than 1 if initializing a new database that already
// has defined migrations.
//
// Some data files have only one table in which case they may inherit the data
// file name.
func (s *Storage) initialize(cx *db.Connection, version int) error {
	if s.Schemas == nil || len(s.Schemas) == 0 {
		return nil
	}

	return cx.Transact(func(tx *db.Transaction) error {
		for _, t := range s.Schemas {
			err := db.Create(tx, t)
			if err != nil {
				return err
			}
		}
		return tx.UpdateSchemaVersion(version)
	})
}

// migrate checks user schema version of connected database and compares
// it to the list of migrations. The length of the migration array is used as
// the data file schema version.
//
// An un-initialized data file has a schema version of 0. A data file with no
// migrations has a version of 1. Four migrations would yield version 5.
//
// If there are more migrations defined than the schema version, the outstanding
// migrations are executed in a transaction to update the schema version.
func (s *Storage) migrate() error {
	if s.Schemas == nil || len(s.Schemas) == 0 {
		return nil
	}

	cx, err := s.Connect()
	if err != nil {
		return err
	}

	version := 1
	if s.Migrations != nil {
		version += len(s.Migrations)
	}

	log.Printf("%s schema at version %d (should be %d)", s.FileName, cx.SchemaVersion, version)

	if cx.SchemaVersion == 0 {
		return s.initialize(cx, version)
	}

	if cx.SchemaVersion >= version {
		return nil
	}

	lastMigration := cx.SchemaVersion - 1

	return cx.Transact(func(tx *db.Transaction) error {
		for _, m := range s.Migrations[lastMigration:] {
			for _, sql := range m {
				_, err := tx.Exec(sql.String())
				if err != nil {
					return err
				}
			}
		}
		return tx.UpdateSchemaVersion(version)
	})
}

// Exec runs a non-row returning query and returns either an inserted row ID
// or the deleted or updated row count.
func (s *Storage) Exec(q db.ExecQuery) (int64, error) {
	cx, err := s.Connect()
	if err != nil {
		return 0, err
	}
	return q.Run(cx)
}

// ExecResponse returns result of query execution as a module Response struct.
func (s *Storage) ExecResponse(q db.ExecQuery) *Response {
	id, err := s.Exec(q)
	if err != nil {
		log.Printf("%v", err)
		return Error(DatabaseError)
	}
	return Success(id)
}

// Rows returns an array of interfaces marshalled from matching records.
func (s *Storage) Rows(q db.RowsQuery, fn db.RowReceiver, vars ...interface{}) ([]interface{}, error) {
	cx, err := s.Connect()
	if err != nil {
		return nil, err
	}
	return q.Into(cx, fn, vars...)
}

func (s *Storage) RowsResponse(q db.RowsQuery, fn db.RowReceiver, vars ...interface{}) *Response {
	list, err := s.Rows(q, fn, vars...)
	if err != nil {
		return Error(DatabaseError)
	}
	return Success(list)
}

// First populates a set of variables with the column values of the first
// matching record.
func (s *Storage) First(q db.RowsQuery, vars ...interface{}) error {
	cx, err := s.Connect()
	if err != nil {
		return err
	}
	return q.First(cx, vars...)

}

// Transact creates a database transaction around a callback function.
func (s *Storage) Transact(fn db.WithTx) error {
	cx, err := s.Connect()
	if err != nil {
		return err
	}
	return cx.Transact(fn)
}
