package module_test

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"toba.tech/app/lib/config"
	"toba.tech/app/lib/db"
	"toba.tech/app/lib/db/schema"
	"toba.tech/app/lib/module"
)

var (
	table1 = schema.Create("table1").Columns(
		schema.DateColumn("col1"),
	)

	table2 = schema.Create("table2").Columns(
		schema.TextColumn("col2"),
	)

	updateTables = schema.Migrate(
		table1.Add(schema.TextColumn("col3").Default("col3")),
		table2.Add(schema.TextColumn("col4").Default("col4")),
	)

	testDB     = module.Database("testdb", table1, table2)
	testModule = module.Register(0, "module_test")
)

// TestMain creates the data file and tables for all other `module_test` tests.
// Regular tests are only run if setup completes and sets status code to 0.
func TestMain(m *testing.M) {
	code := 1
	defer func() {
		if code == 0 {
			code = m.Run()
		}
		os.Exit(code)
	}()

	dir, err := ioutil.TempDir(os.TempDir(), "toba")
	if err != nil {
		log.Printf("Failed to create temporary directory\n%v\n", err)
		return
	}

	defer os.Remove(dir)

	_, err = os.Stat(dir)
	if err != nil {
		log.Printf("Failed to get %s details\n%v\n", dir, err)
		return
	}

	dataFilePath := filepath.Join(dir, "toba")

	err = db.Initialize(config.Database{Path: dir})
	if err != nil {
		log.Printf("Failed to initialize database at %s\n%v\n", dir, err)
		return
	}

	_, err = db.ConnectToPath(dataFilePath)
	if err != nil {
		log.Printf("Failed to connect to %s\n%v\n", dataFilePath, err)
		return
	}

	code = 0
}

func TestMigrationWithoutTables(t *testing.T) {
	err := testModule.Migrate()
	assert.NoError(t, err)
}

func TestBasicTableMigration(t *testing.T) {
	testModule.Storage(testDB)
	err := testModule.Migrate()
	assert.NoError(t, err)

	cx, err := testDB.Connect()
	assert.NoError(t, err)

	if cx != nil {
		assert.True(t, cx.HasTable(table1.Name))
		assert.True(t, cx.HasTable(table2.Name))
		assert.False(t, cx.HasTable("nothing"))
	}
}

func TestFullMigrations(t *testing.T) {
	testDB.Migrations = []schema.Migration{updateTables}
	err := testModule.Migrate()
	assert.NoError(t, err)

	cx, err := testDB.Connect()
	assert.NoError(t, err)

	// verify new columns can be selected
	rows, err := db.Select("*").From(table1).Where("col3 IS NOT NULL").Run(cx)
	assert.NoError(t, err)
	rows.Close()

	rows, err = db.Select("*").From(table2).Where("col4 IS NOT NULL").Run(cx)
	assert.NoError(t, err)
	rows.Close()
}
