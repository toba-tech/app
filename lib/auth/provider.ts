/**
 * Maybe auto-generate later but for now match keys defined in Go provider
 * structs.
 */
export enum Provider {
   Amazon = "amazon",
   Dropbox = "dropbox",
   Facebook = "facebook",
   GitHub = "github",
   Google = "google",
   Yahoo = "yahoo"
}

export interface AuthLink {
   id:number;
   key:string;
   url:string;
   selected:boolean;
}