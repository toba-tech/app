package oops

import "errors"

var (
	// ErrNotImplemented indicates a method is stubbed but not implemented.
	NotImplemented = errors.New("inteface method not implemented")

	// ErrNotFound is returned when the specified record is not saved in the bucket.
	NotFound = errors.New("not found")

	// ErrAlreadyExists is returned uses when trying to set an existing value on a field that has a unique index.
	AlreadyExists = errors.New("already exists")

	InvalidItemKey = errors.New("invalid item key")

	InvalidIndexKey = errors.New("invalid index key")

	// ErrIncompatibleValue returns when a bucket name is too short or long.
	InvalidBucketName = errors.New("invalid bucket name")
	// ErrNotNumeric returns when an interface{} parameter value is expected to be numeric.
	NotNumeric = errors.New("not a number")
	// ErrStructPtrNeeded is returned when an unexpected value is given, instead of a pointer to struct.
	StructPtrNeeded = errors.New("argument must be pointer to struct")

	// ErrNilParam is returned when the specified param is expected to be not nil.
	NilParam = errors.New("param must not be nil")
)
