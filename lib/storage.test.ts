/* tslint:disable:no-unused-expression */
import "mocha";
import { expect } from "chai";
import { session } from "./storage";

describe("Session", () => {
   it.skip("saves numbers", ()=> {
      session.save("test-key", 99);
      expect(session.item("test-key")).equals(99);
   });
});