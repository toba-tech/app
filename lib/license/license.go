package license

type (
	License struct {
		Name  string
		Valid bool
	}
)

func Load() (*License, error) {
	return &License{"none", false}, nil
}
