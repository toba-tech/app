package ldap

const (
	FieldAccountExpires  = "accountExpires"
	FieldCommonName      = "cn"
	FieldDepartment      = "department"
	FieldDirectoryPath   = "distinguishedName"
	FieldDisplayName     = "displayName"
	FieldEmail           = "mail"    // check
	FieldEnabled         = "enabled" // check -- only present if disabled?
	FieldGivenName       = "givenName"
	FieldLastLogon       = "lastLogon" // check
	FieldLogonCount      = "logonCount"
	FieldGroups          = "memberOf"
	FieldMobile          = "mobile"
	FieldName            = "name"
	FieldDirectoryID     = "objectGUID"
	FieldLocation        = "physicalDeliveryOfficeName"
	FieldPhoto           = "thumbnailPhoto"
	FieldPasswordLastSet = "pwdLastSet"
	FieldPhone           = "telephoneNumber"
	FieldTitle           = "title"
	FieldType            = "objectClass"
	FieldAccountName     = "sAMAccountName"
)

// Active Directory account types
// https://msdn.microsoft.com/en-us/library/ms679637%28v=vs.85%29.aspx
const (
	TypeDomain           = 0x0
	TypeGroup            = 0x10000000
	TypeNonSecurityGroup = 0x10000001
	TypeAlias            = 0x20000000
	TypeNonSecurityAlias = 0x20000001
	TypeUser             = 0x30000000
	TypeMachine          = 0x30000001
	TypeTrustAccount     = 0x30000002
	TypeAppBasicGroup    = 0x40000000
	TypeAppQueryGroup    = 0x40000001
	TypeAccountMax       = 0x7fffffff
)

var defaultFields = []string{
	FieldAccountName,
	FieldName,
	FieldTitle,
	FieldDepartment,
	FieldEmail,
	FieldLocation,
	FieldPhone,
	FieldMobile,
}
