// https://github.com/korylprince/go-ad-auth
// https://github.com/jtblin/go-ldap-client
package ldap

import (
	"crypto/tls"
	"errors"
	"log"
	"strconv"
	"strings"

	"gopkg.in/ldap.v2"
	"toba.tech/app/lib/config"
)

var (
	c                     config.LDAP
	domain                string
	tlsConfig             *tls.Config
	ErrInvalidBaseDN      = errors.New("invalid base DN")
	ErrEmptyPassword      = errors.New("password is empty")
	ErrInvalidCredentials = errors.New("invalid credentials")
)

func Initialize(configuration config.LDAP) error {
	c = configuration

	log.Printf("Initializing LDAP connection to %s/%s", c.Server, c.BaseDN)

	tlsConfig = &tls.Config{ServerName: c.Server}
	c.Server = c.Server + ":" + strconv.FormatUint(uint64(c.Port), 10)

	d, err := domainForDN()
	if err != nil {
		return err
	}
	domain = d

	cx, err := connect()
	if err != nil {
		return err
	}
	cx.Close()

	return nil
}

// SecurityType specifies how to connect to an Active Directory server.
type SecurityType int

// Security will default to SecurityNone if not given.
const (
	SecurityNone SecurityType = iota
	SecurityTLS
	SecurityStartTLS
)

// Connect returns an open connection to an Active Directory server specified by
// the given config.
func connect() (*ldap.Conn, error) {
	// switch c.Security {
	// case SecurityNone:
	cx, err := ldap.Dial("tcp", c.Server)
	if err != nil {
		return nil, err
	}

	err = cx.Bind(c.UserName+"@"+domain, c.Password)
	if err != nil {
		return nil, err
	}
	return cx, nil
}

// case SecurityTLS:
// 	conn, err := ldap.DialTLS("tcp", fmt.Sprintf("%s:%d", c.Server, c.Port), c.TLSConfig)
// 	if err != nil {
// 		if c.Debug {
// 			log.Printf("DEBUG: LDAP Error %v\n", err)
// 		}
// 		return nil, err
// 	}
// 	return conn, nil
// case SecurityStartTLS:
// 	conn, err := ldap.Dial("tcp", fmt.Sprintf("%s:%d", c.Server, c.Port))
// 	if err != nil {
// 		if c.Debug {
// 			log.Printf("DEBUG: LDAP Error %v\n", err)
// 		}
// 		return nil, err
// 	}
// 	err = conn.StartTLS(c.TLSConfig)
// 	if err != nil {
// 		if c.Debug {
// 			log.Printf("DEBUG: LDAP Error %v\n", err)
// 		}
// 		return nil, err
// 	}
// 	return conn, nil
// default:
// 	return nil, ConfigError("Invalid Security setting")
// }
//}

func FindUser(accountName string, fields ...string) (*ldap.Entry, error) {
	if fields == nil {
		fields = defaultFields
	}
	entries, err := search(&ldap.SearchRequest{
		BaseDN:       c.BaseDN,
		Scope:        ldap.ScopeWholeSubtree,
		DerefAliases: ldap.DerefAlways,
		TypesOnly:    false,
		Filter:       "(" + FieldAccountName + "=" + accountName + ")",
		SizeLimit:    400,
		Attributes:   fields,
	})

	if err != nil {
		return nil, err
	}
	return entries[0], nil
}

func search(req *ldap.SearchRequest) ([]*ldap.Entry, error) {
	cx, err := connect()
	if err != nil {
		return nil, err
	}
	defer cx.Close()
	res, err := cx.Search(req)

	if err != nil {
		return nil, err
	}
	return res.Entries, nil
}

func domainForDN() (string, error) {
	d := ""
	for _, v := range strings.Split(strings.ToLower(c.BaseDN), ",") {
		if trimmed := strings.TrimSpace(v); strings.HasPrefix(trimmed, "dc=") {
			d = d + "." + trimmed[3:]
		}
	}
	if len(d) <= 1 {
		return "", ErrInvalidBaseDN
	}
	return d[1:], nil
}

// func getDN(cn string, config *Config, conn *ldap.Conn) (string, error) {
// 	search := ldap.NewSearchRequest(
// 		config.BaseDN,
// 		ldap.ScopeWholeSubtree,
// 		ldap.DerefAlways,
// 		1, 0,
// 		false,
// 		fmt.Sprintf("(cn=%s)", cn),
// 		nil,
// 		nil,
// 	)
// 	result, err := conn.Search(search)
// 	if err != nil {

// 		return "", err
// 	}
// 	if len(result.Entries) > 0 {
// 		return result.Entries[0].DN, nil
// 	}
// 	return "", ConfigError(fmt.Sprintf("No DN found for: %s", cn))
// }

// func attrsToMap(entry *ldap.Entry) map[string][]string {
// 	m := make(map[string][]string)
// 	for _, attr := range entry.Attributes {
// 		m[attr.Name] = attr.Values
// 	}
// 	return m
// }

// func inGroup(username, group string, config *Config, conn *ldap.Conn, attrs []string) (bool, map[string][]string, error) {
// 	groupDN, err := getDN(group, config, conn)
// 	if err != nil {
// 		if config.Debug {
// 			log.Printf("DEBUG: Error: %s\n", err)
// 		}
// 		return false, nil, err
// 	}
// 	search := ldap.NewSearchRequest(
// 		config.BaseDN,
// 		ldap.ScopeWholeSubtree,
// 		ldap.DerefAlways,
// 		1, 0,
// 		false,
// 		fmt.Sprintf("(sAMAccountName=%s)", username),
// 		append(attrs, "memberOf"),
// 		nil,
// 	)
// 	result, lErr := conn.Search(search)
// 	if lErr != nil {
// 		if config.Debug {
// 			log.Printf("DEBUG: LDAP Error %v\n", lErr)
// 		}
// 		return false, nil, lErr
// 	}
// 	if len(result.Entries) == 1 {
// 		entryAttrs := attrsToMap(result.Entries[0])
// 		if groups, ok := entryAttrs["memberOf"]; ok {
// 			for _, g := range groups {
// 				if groupDN == g {
// 					for _, key := range attrs {
// 						if key == "memberOf" {
// 							return true, entryAttrs, nil
// 						}
// 					}
// 					delete(entryAttrs, "memberOf")
// 					return true, entryAttrs, nil
// 				}
// 			}
// 		}
// 		return false, entryAttrs, nil
// 	}
// 	return false, nil, LDAPError("Amount of Entries returned was not one")
// }

// func getAttrs(username string, config *Config, conn *ldap.Conn, attrs []string) (map[string][]string, error) {
// 	search := ldap.NewSearchRequest(
// 		config.BaseDN,
// 		ldap.ScopeWholeSubtree,
// 		ldap.DerefAlways,
// 		1, 0,
// 		false,
// 		fmt.Sprintf("(sAMAccountName=%s)", username),
// 		attrs,
// 		nil,
// 	)
// 	result, lErr := conn.Search(search)
// 	if lErr != nil {
// 		if config.Debug {
// 			log.Printf("DEBUG: LDAP Error %v\n", lErr)
// 		}
// 		return nil, lErr
// 	}
// 	if len(result.Entries) == 1 {
// 		return attrsToMap(result.Entries[0]), nil
// 	}
// 	return nil, LDAPError("Amount of Entries returned was not one")
// }

func Login(accountName, password string, fields ...string) (*ldap.Entry, error) {
	if password == "" {
		return nil, ErrEmptyPassword
	}
	cx, err := connect()
	if err != nil {
		return nil, err
	}
	defer cx.Close()

	err = cx.Bind(accountName+"@"+domain, password)
	if err != nil {
		if e, ok := err.(*ldap.Error); ok {
			if e.ResultCode == ldap.LDAPResultInvalidCredentials {
				return nil, ErrInvalidCredentials
			}
		}
		return nil, err
	}
	return FindUser(accountName, fields...)
}

func normalize(entry *ldap.Entry, exclude ...string) (*ldap.Entry, error) {
	return nil, nil
}
