// Generated by /cmd/generate/main.go

/* tslint:disable:trailing-comma */
export enum OperatingSystem {
    Linux = 1,
    MacOS = 2,
    Unknown = 0,
    Windows = 3,
}
/* tslint:enable:trailing-comma */
