// Package host defines methods for interacting with the host operating system.
// It can launch the default handler for a URL, an adaptation of
// https://github.com/skratchdot/open-golang whicn in turn is a port of
// https://github.com/pwnall/node-open.
//
// It is equivalent to the following commands:
//	         OSX: "open"
//	     Windows: "start"
//	 Linux/Other: "xdg-open"
package host

import (
	"net"
	"strconv"
)

type OperatingSystem int

const (
	Unknown OperatingSystem = iota
	Linux
	MacOS
	Windows
)

// Run opens a file, directory or URI with the OS default application and waits
// for the action to complete before returning.
func Run(input string) error {
	return open(input).Run()
}

// Run opens a file, directory or URI with the OS default application and
// returns without waiting for the action to complete.
func Start(input string) error {
	return open(input).Start()
}

// Run opens a file, directory or URI with the named application and waits
// for the action to complete before returning.
func RunWith(input string, appName string) error {
	return openWith(input, appName).Run()
}

// Run opens a file, directory or URI with the named application and
// returns without waiting for the action to complete.
func StartWith(input string, appName string) error {
	return openWith(input, appName).Start()
}

// IsPortAvailable tests whether a network part is already in use.
func IsPortAvailable(port uint) bool {
	listener, err := net.Listen("tcp", ":"+strconv.FormatUint(uint64(port), 10))
	if err != nil {
		return false
	}
	return listener.Close() == nil
}

// FirstAvailablePort iterates over port numbers and returns the first one that
// is unused.
func FirstAvailablePort(ports ...uint) uint {
	for _, p := range ports {
		if IsPortAvailable(p) {
			return p
		}
	}
	return 0
}
