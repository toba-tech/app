// +build !windows,!darwin

package host

import "os/exec"

// open executes a bash script to open a named file or URI with the OS default
// application.
//
// http://sources.debian.net/src/xdg-utils/1.1.0~rc1%2Bgit20111210-7.1/scripts/xdg-open/
// http://sources.debian.net/src/xdg-utils/1.1.0~rc1%2Bgit20111210-7.1/scripts/xdg-mime/
func open(input string) *exec.Cmd {
	return exec.Command("xdg-open.sh", input)
}

// open executes an application to open a named file or URI.
func openWith(input string, appName string) *exec.Cmd {
	return exec.Command(appName, input)
}
