package host_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"toba.tech/app/lib/host"
)

const (
	validURL    = "https://google.com/"
	invalidURL  = "[<Invalid URL>]"
	specificApp = "firefox"
)

func TestRun(t *testing.T) {
	assert.NoError(t, host.Run(validURL))
	assert.NoError(t, host.Run("http://localhost/"))
	assert.Error(t, host.Run(invalidURL))
}

func TestStart(t *testing.T) {
	assert.NoError(t, host.Start(validURL))
	assert.Error(t, host.Start(invalidURL))
}

func TestRunWith(t *testing.T) {
	assert.NoError(t, host.RunWith(validURL, specificApp))
	assert.Error(t, host.RunWith(invalidURL, specificApp))
}

func TestStartWith(t *testing.T) {
	assert.NoError(t, host.StartWith(validURL, specificApp))
	assert.Error(t, host.StartWith(invalidURL, specificApp))
}
