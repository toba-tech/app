let w:Worker = null;

/**
 * Ensure worker exists.
 */
function ensureWorker():Worker {
   if (w === null) { w = new Worker("js/worker.js"); }
   return w;
}

/**
 * Create web worker thread to process data. Always append timestamp to be
 * re-hydrated by server/middleware.js#webWorker().
 */
export function spawn(threadType:Type, data:any) {
   data.timestamp = new Date();
   ensureWorker().postMessage({ threadType, data });
}

export enum Type {
   Log = 1,
   Activity
}