import { spawn, Type as ThreadType } from "./thread";

/**
 * Spawn a web worker to record log.
 */
function log(level:number, message:string, data:any = null) {
   spawn(ThreadType.Log, { level, message, data });
}
function debug(msg:string) { log(Type.Debug, msg); }
function info(msg:string) { log(Type.Info, msg); }
function warn(msg:string) { log(Type.Warn, msg); }

/**
 * Log an error message with additional details.
 */
function error(msg:string, data:any = null) {
   if (data == null && typeof(msg) == "object") {
      data = msg;
      msg = null;
   }
   //log(Type.Error, msg, data);
   console.error(msg, data);
}

function activity(type:Activity, data:any) {
   spawn(ThreadType.Activity, { type, data, userID: "none" });
}

function appStart(moduleID:number) { activity(Activity.StartApp, { moduleID }); }
function appExit(moduleID:number) { activity(Activity.ExitApp, { moduleID }); }
function newView(name:string) { activity(Activity.LoadView, { name }); }

export enum Type {
   Debug = 1,
   Info,
   Warn,
   Error
}

export enum Activity {
   StartApp = 1,
   ExitApp,
   LoadView
}

export default {
   debug,
   info,
   warn,
   error,
   activity,
   appStart,
   appExit,
   newView
};