// Package config loads JSON configuration files into struct. The configuration
// file is loaded from the same path as the executable unless the environment
// variable TOBA_PATH is set.
package config

import (
	"encoding/json"
	"os"
	"path/filepath"
	"regexp"

	"toba.tech/app/lib/web/file"
)

// Server configuration
type (
	Database struct {
		// Path to all database files.
		Path string `json:"path"`
		// Name of shared database file.
		Name string `json:"name"`
	}
	LDAP struct {
		Server   string `json:"server"`
		Port     uint   `json:"port"`
		UserName string `json:"username"`
		Password string `json:"password"`
		BaseDN   string `json:"baseDN"`
	}
	HTTP struct {
		// SslCert is the path and name of the SSL certificate file.
		SslCert string `json:"sslCert"`
		// SslKey is the path and name of the SSL key file.
		SslKey string `json:"sslKey"`
		Port   int    `json:"port"`
		// FromZip is the name of a zip file to serve content from rather
		// than the file system.
		FromZip string `json:"fromZip"`
		// FromFolder is the folder containing web content.
		FromFolder string `json:"fromFolder"`
		// SyncFileAccess indicates if RWMutex lock should be used when reading
		// the file cache. It should only be true while debugging when files might
		// be changing while the web server is active.
		SyncFileAccess bool
	}
	Server struct {
		Database Database `json:"database"`
		LDAP     LDAP     `json:"ldap"`
		HTTP     HTTP     `json:"http"`
	}
)

// Load reads settings from JSON file. The file will be loaded from the
// working directory or, if not found, will be generated with default values.
func Load() (Server, error) {
	createFile := false
	config := Server{}
	file, err := file.Open("toba.config")
	if err == nil {
		decoder := json.NewDecoder(file)
		_ = decoder.Decode(&config)
		// if err != nil {
		// 	return config, err
		// }
	} else {
		createFile = true
		err = nil
	}
	re := regexp.MustCompile("^./")
	d := &config.Database
	web := &config.HTTP
	ldap := &config.LDAP

	// normalize values
	if ldap.Port == 0 {
		ldap.Port = 389
	}
	if ldap.UserName == "" {
		ldap.UserName = os.Getenv("TOBA_LDAP_USER")
	}
	if ldap.Password == "" {
		ldap.Password = os.Getenv("TOBA_LDAP_PASSWORD")
	}
	if d.Name == "" {
		d.Name = "toba"
	}
	if d.Path == "" {
		d.Path = "data"
	} else {
		// unlike ES, Go local paths do not being with ./
		d.Path, _ = filepath.Abs(re.ReplaceAllString(d.Path, ""))
	}
	if web.Port == 0 {
		web.Port = 443
	}

	web.SyncFileAccess = os.Getenv("TOBA_DEBUG") == "true"
	web.FromFolder = re.ReplaceAllString(web.FromFolder, "")
	web.SslCert = re.ReplaceAllString(web.SslCert, "")
	web.SslKey = re.ReplaceAllString(web.SslKey, "")

	if createFile {
		content, err := json.MarshalIndent(config, "", "   ")
		if err != nil {
			return config, err
		}
		file, err = os.Create("toba.config")
		if err != nil {
			return config, err
		}

		_, err = file.Write(content)
		if err != nil {
			return config, err
		}
	}

	return config, err
}
