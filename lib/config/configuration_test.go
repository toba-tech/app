package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"toba.tech/app/lib/config"
)

func TestServerConfig(t *testing.T) {
	c, err := config.Load()
	assert.NoError(t, err)
	assert.Equal(t, c.Database.Name, "toba")
}
