/*
   Web worker to queue and send messages to server.
*/
import "whatwg-fetch";

const mimeType = "application/json";
/** Milliseconds to wait for sending message */
const interval = 10000;
/** Whether message is currently being sent */
let sending = false;
let timer:number;
let pending:any[] = [];

// web worker message event
//self.addEventListener('message', (e:Event) => { queue(e.data); }, false);

/**
 * Queue a message to be sent.
 */
export default function queue(msg:any) {
   pending.push(msg);
   if (timer == 0) { timer = window.setTimeout(send, interval); }
}

/**
 * Send queued messages.
 */
function send() {
   if (sending) { return; }
   // copy pending in case other messages arrive during POST
   const toSend = pending.slice(0);
   pending = [];
   sending = true;
   fetch("/worker", {
      method: "POST",
      headers: {
         Accept: mimeType,
         "Content-Type": mimeType
      },
      body: JSON.stringify(toSend)
   }).then(response => {
      if (response.status < 200 || response.status > 299) {
         throw new Error(response.toString());
      }
      sending = false;
   }).catch(err => {
      console.error(err);
      // put send failures back in pending queue
      pending = pending.concat(toSend);
      sending = false;
   });
}