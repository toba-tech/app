import { PhraseList } from "./i18n/localize";

export const phrases:PhraseList = {
   ABOUT: {
      en: "About"
   },
   COMMUNICATIONS: {
      en: "Communications"
   },
   CONTINUE: {
      en: "Continue"
   },
   EMPLOYEE_DIRECTORY: {
      en: "Employee Directory"
   },
   FORUM: {
      en: "Forum"
   },
   INCIDENT_MANAGEMENT: {
      en: "Incident Management"
   },
   KB: {
      en: "Knowledge Base"
   },
   NO_WEBSOCKET_RESPONSE: {
      en: "The server isn’t responding. Is it still there?"
   },
   PRIVACY: {
      en: "Privacy"
   },
   QUERY_ERROR: {
      en: "Uh oh, the server is talking gibberish"
   },
   SECURITY: {
      en: "Security"
   },
   SERVICE_DESK: {
      en: "Service Desk"
   },
   SYSTEM: {
      en: "System"
   }
};

export interface Translation {
    WEB_SERVER_LISTENING:string;
}

export default phrases;