import { Service } from "lib/module/service";
import { Status } from "lib/module/status";
import { is, randomID, removeItem } from "lib/utility";
import text from "lib/i18n/localize";
import log from "lib/logger";

export interface ConnectionState {
   /** Wether server web socket connection is offline. */
   offline:boolean;
   /**
    * Number of seconds until the connection is automatically retried. Value
    * will be zero if no automatic retries remain.
    */
   secondsUntilRetry:number;
}

/**
 * OfflineHandler receives connection state updates to display user messages
 * or otherwise control UX.
 */
export type OfflineHandler = (s:ConnectionState)=>void;

/**
 * ConnectionResolver is a callback accepting a reconnected WebSocket.
 */
type ConnectionResolver = (ws:WebSocket|PromiseLike<WebSocket>)=>void;

/**
 * Promise-like subscription to a WebSocket request. Every WebSocket call is
 * made a subscription that may `resolve` or `reject`.
 */
interface Subscription {
   resolve:any|PromiseLike<any>;
   reject(msg:any):void;
   /** SetTimeout timer to handle overdue responses */
   timer:number;
}

/**
 * Handler encapsulates WebSocket response subscriptions and offline handlers.
 */
const handler = {
   /** Subscriber objects keyed to random ID sent with each call. */
   response: {} as {[key:string]:Subscription},
   /** Callbacks subscribed to offline events. */
   offline: [] as OfflineHandler[],
   /** Send offline notification to subscribed handlers. */
   notify(secondsUntilRetry = 0, offline = true) {
      this.offline.forEach(h => { h({ offline,  secondsUntilRetry }); });
   }
};

/**
 * Manage socket reconnection with initial, invisible retries that escalate to
 * a visible warning with slower retries and ultimately a link to retry but no
 * further automatic retries.
 *
 * https://github.com/joewalnes/reconnecting-websocket/blob/master/reconnecting-websocket.js
 */
const reconnect = {
   retryDelays: [2, 3, 5, 8, 13, 21, 34, 55],
   timer: 0,
   attempts: 0,
   resolvers: [] as ConnectionResolver[],
   rejectors: [] as ((reason?:any)=>void)[],

   /**
    * Add promise callbacks for connection attempts during retries.
    */
   queue(resolve:ConnectionResolver, reject:(reason?:any)=>void) {
      this.resolvers.push(resolve);
      this.rejectors.push(reject);
   },

   try(e?:CloseEvent) {
      readyState = WebSocket.CONNECTING;

      if (e !== undefined && e.wasClean) {
         handler.notify(0, false);
         this.done();
         return;
      }

      if (this.attempts > this.retryDelays.length) {
         this.fail();
         return;
      }

      const wait = this.retryDelays[this.attempts];
      handler.notify(wait, true);

      this.attempts++;
      this.timer = window.setTimeout(()=> { connect(true); }, wait * 1000);
   },

   /**
    * Immediately retry connection and cancel timer without consuming any of
    * the automatic retries.
    */
   now() {
      window.clearTimeout(this.timer);
      handler.notify(0, true);
      connect(true);
   },

   /**
    * Resolve connection retries.
    */
   finish(ws:WebSocket) {
      readyState = WebSocket.OPEN;
      handler.notify(0, false);
      this.resolvers.forEach(r => r(ws));
      this.done();
   },

   /**
    * Mark connection as closed and notify subscriptions of failure.
    */
   fail(e?:Event) {
      readyState = WebSocket.CLOSED;
      handler.notify(0, true);
      this.rejectors.forEach(r => r(e));
      this.done();
   },

   /**
    * Clean-up reconnection state.
    */
   done() {
      this.attempts = 0;
      this.resolvers = [];
      this.rejectors = [];
      window.clearTimeout(this.timer);
   }
};

/** WebSocket.`CONNECTING`, `OPEN`, `CLOSING` or `CLOSED`. */
let readyState = WebSocket.CLOSED;
/** Current connection. */
let ws:WebSocket = null;
let jwt:string = null;

/**
 * Send query over websocket. The call is assigned a random ID so its response
 * can be matched back to the caller after `Promise` resolution on the
 * `onmessage` handler below.
 *
 * https://github.com/gorilla/websocket/blob/master/examples/chat/home.html
 */
export function call<T>(type:number, data:T = null, subscribe = false) {
   return new Promise((resolve, reject) => {
      const id = randomID();
      connect().then(ws => {
         const req:Service.Request<T> = { id, jwt, type, data, subscribe };
         ws.send(JSON.stringify(req));
         // create a subscriber object to fulfill the promise or timeout
         handler.response[id] = {
            resolve,
            reject,
            timer: window.setTimeout(() => {
               delete handler.response[id];
               reject(text.NO_WEBSOCKET_RESPONSE);
            }, 5000)
         } as Subscription;
      });
   });
}

/**
 * Subscribe a callback to be notified when the websocket connection goes
 * offline.
 */
export function addOfflineHandler(h:OfflineHandler) {
   handler.offline.push(h);
}

export function removeOfflineHandler(h:OfflineHandler) {
   removeItem(handler.offline, h);
}

/**
 * Immediately retry websocket connection if it's in a disconnected but not
 * closed state.
 */
export function retryConnection() {
   if (readyState == WebSocket.CONNECTING) {
      reconnect.now();
   }
}

/**
 * Set the token that will be used for subsequent requests.
 */
export function updateToken(token:string) { jwt = token; }

/**
 * Connect WebSocket and process incoming messages. If connection is offline
 * then new client calls will be added to a queue until the retry succeeds or
 * fails.
 */
const connect = (retrying = false) => new Promise((resolve, reject) => {
   if (readyState == WebSocket.CONNECTING && !retrying) {
      reconnect.queue(resolve, reject);
   } else if (readyState != WebSocket.OPEN) {
      const url = location.origin.replace(/^http/, "ws") + "/ws";
      ws = new WebSocket(url);
      ws.onopen = ()=> {
         reconnect.finish(ws);
         resolve(ws);
      };
      ws.onclose = (e:CloseEvent) => {
         reconnect.try(e);
      };
      ws.onerror = (e:Event) => {
         if (!retrying) {
            log.error(`Unable to open web socket to ${url}`, e);
            reject(e);
         }
      };
      ws.onmessage = (e:MessageEvent) => {
         const msg:Service.Response = JSON.parse(e.data);
         if (is.defined(handler.response, msg.id)) {
            // update subscriber promise then remove subscriber
            const s = handler.response[msg.id];
            if (msg.status == Status.Okay) {
               s.resolve(msg.data);
            } else {
               s.reject(msg.status);
            }
            clearTimeout(s.timer);
            delete handler.response[msg.id];
         } else {
            log.error(`No WebSocket request matches ${msg.id}`);
         }
      };
   } else {
      resolve(ws);
   }
}) as Promise<WebSocket>;