declare namespace WebToken {
   export interface Header {
      /** Media type */
      typ?:string;
      /** Content type */
      cty?:string;
      /** Algorithm */
      alg:string;
   }

   export interface Payload {
      /** Issuer */
      iss?:string;
      /** Expiration seconds since 1970 */
      exp?:number;
      /** Subject */
      sub?:string;
      /** Audience */
      aud?:string;
      /**
       * "Not Before Seconds" since 1970.
       * The time before which the JWT MUST NOT be accepted for processing.
       * The processing of the "nbf" claim requires that the current date/time
       * MUST be after or equal to the not-before date/time listed in the claim.
       */
      nbf?:number;
      /** "Issued at" seconds since 1970 */
      iat?:number;
      /** ID */
      jti?:string;
   }

   export interface Options {
      header:Header;
      payload:Payload;
   }
}

export default WebToken;