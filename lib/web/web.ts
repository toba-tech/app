import { Stream } from "stream";

export namespace Web {
   type Method = "GET" | "HEAD" | "OPTIONS" | "POST" | "PUT" | "UPDATE" | "DELETE";

   export interface Context {
      /** Whether request header indicates character set is accepted */
      acceptsCharset(set:string):boolean;
      /** Whether request header indicates coding is accepted */
      acceptsEncoding(encoding:string):boolean;
      /** Whether request header indicates MIME type is accepted */
      acceptsType(type:string):boolean;
      /** Set header value(s) */
      set(key:string, value:HeaderValue):Context;
      set(hash:{[key:string]:HeaderValue}):Context;
      /** Remove header values */
      remove(...field:string[]):Context;

      url:string;
      method:Method;
      path:string;
      //protocol:string;
      /** Whether connection uses TLS */
      //secure:boolean;
      /** Request content type */
      type:string;
      /** Status code */
      status:number;

      body:string|Buffer|Stream;
      ip:string;
   }

   export interface Middleware {
      (ctx:Context):Promise<Context>;
   }

   export type HeaderValue = string|number|boolean;
   export type Body = string|Buffer|Stream;

   namespace Worker {
      export interface Message {
         threadType:number;
         data:any;
      }
   }

   interface Options {
      origin?:string;
      methods?:string;
      credentials?:boolean;
      maxAge?:number;
      headers?:string;
      root?:string;
      gzip?:boolean;
   }
}