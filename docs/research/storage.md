# Gun
Ideas seem okay but founder is gooofy 

#LevelUP
[Derivitives](https://github.com/Level/levelup/wiki/Modules)
Most of the derivitives aren't maintained

# Postgres
Performant but harder to install, manage and interact with

# PouchDB
[Misconceptions about PouchDB](https://gist.github.com/nolanlawson/dc80e449079c2bc33170)

# Realm
[Node version](https://realm.io/news/first-object-database-realm-node-js-server/) is free now but Enterprise version is usually [$1,500 per month](https://realm.io/pricing/)

# RethinkDB
[No longer maintained](https://news.ycombinator.com/item?id=12649414)
But maybe use server-side without Horizon
[GoRethink](https://github.com/GoRethink/gorethink) is also deprecated but could be forked.
Use [Rethinkdbdash](https://github.com/neumino/rethinkdbdash)

# RQLite
Distributed database built on SQLite.

# SQLite
Requires CGO but maybe [cznic's automated port to Go](https://github.com/cznic/sqlite) will succeed.